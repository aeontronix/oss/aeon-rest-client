/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.restclient;

import com.aeontronix.commons.ReflectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Note: test endpoints from https://badssl.com/
 */
public class RESTClientSimpleTests {
    private String url;
    private RESTClient restClient;

    @BeforeEach
    public void init() {
        restClient = RESTClient.builder().insecure().build();
    }

    @Test
    public void testGetJson() throws RESTException {
        Map<?, ?> json = restClient.get("https://dummyjson.com/test", Map.class);
        HashMap<Object, Object> expected = new HashMap<>();
        expected.put("status", "ok");
        expected.put("method", "GET");
        Assertions.assertEquals(json, expected);
    }

    private static final Logger logger = getLogger(RESTClientSimpleTests.class);

    @Test
    public void testWireDebug() throws Exception {
        ReflectionUtils.set(getLogger(RESTRequest.class), "currentLogLevel", 10);
        Map<?, ?> json = restClient.post("https://dummyjson.com/test")
                .entity(new BufferedInputStream(new ByteArrayInputStream("ADSDFSAFADS".getBytes())))
                .executeAndConvertToObject(Map.class);
    }
}

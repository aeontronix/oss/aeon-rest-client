/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.restclient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

/**
 * Note: test endpoints from https://badssl.com/
 */
public class RESTClientSSLTests {
    private String url;
    private RESTClient restClient;

    @BeforeEach
    public void init() {
        restClient = RESTClient.builder().insecure().build();
    }

    @Test
    public void testSelfSignedServerCert() throws RESTException {
        restClient.get("https://self-signed.badssl.com/", String.class);
    }

    @Test
    public void test() throws RESTException {
        String s = RESTClient.builder().build().get("http://www.yahoo.com")
                .executeAndConvertToObject(String.class);
//        String s = RESTClient.builder().build().post("http://www.yahoo.com")
//                .entity("HELLO WORLD".getBytes(StandardCharsets.UTF_8)).executeAndConvertToObject(String.class);
        System.out.println(s);
    }
}

# Aeon REST Client

This library is designed to facilitate making calls to REST APIs.

## Features

- Built-in conversion from/to json
- Pagination support
- Automatic error message extraction from json error responses
- Automatic retries, including proper support for retry-after (all calls are blocked until retry-after) 

## Usage

To use this library you need to add it your project. For example using maven add the following to your dependencies
( replace `[LATEST-VERSION}` with the latest version number which can find here: [https://mvnrepository.com/artifact/com.aeontronix.restclient/aeon-rest-client](https://mvnrepository.com/artifact/com.aeontronix.restclient/aeon-rest-client) ])

```xml
<dependency>
    <groupId>com.aeontronix.restclient</groupId>
    <artifactId>aeon-rest-client</artifactId>
    <version>[LATEST-VERSION]</version>
</dependency>
```

Using this dependency will include [Apache HTTP components](https://hc.apache.org/) and
[Jackson](https://github.com/FasterXML/jackson).

Other libraries can be used by importing just the core and other implementations integration libraries

```xml

<dependency>
    <groupId>com.aeontronix.restclient</groupId>
    <artifactId>aeon-rest-client-core</artifactId>
    <version>[LATEST-VERSION]</version>
</dependency>
```

### Basic usage

You can then build a RestClient object using its builder:

```java
RESTClient restClient=RESTClient.builder().build();
        RESTResponse response=restClient.get("https://example.com").execute();
```

You can also directly get the response as an object:

```java
String responseContent=restClient.get("https://example.com/stuff",String.class);
```

It supports automatic conversion of JSON payloads to objects

```java
MyResponseObj responseContent=restClient.get("https://example.com/stuff",MyResponseObj.class);
```

If you are making calls to multiple endpoints, you can get a rest client host object:

```java
RESTClientHost host=restClient.host("http:/").build();
        host.get("/stuff",String.class);
```

### Pagination

For json apis, restclient can automatically handle pagination.

ie:

```json
try (final PaginatedResponse<String> r = restClient.get(API_URL).executePaginated(String.class,"offset", "limit", "/xval")) {
final List<String> results = r.toList();
assertEquals(Arrays.asList("foo", "bar", "baz"), results);
}
```

### Error Handling

RESTClient will automatically throw exceptions when a non-2xx error. You can override default behavior by setting your
own RESTResponseValidator.

```java
RESTClient.builder().responseValidator((RESTResponseValidator)r->{
        // you own validation logic
        }).build();
```

```java
restClient.get("foo").responseValidator((RESTResponseValidator)r->{
        // you own validation logic
        }).executeAndConvertToObject(String.class);
```

### Retry

You can set retries of errors by using the retry parameter:

```java
RESTResponse response=restClient.get("https://myapi").retry(2).execute();
```

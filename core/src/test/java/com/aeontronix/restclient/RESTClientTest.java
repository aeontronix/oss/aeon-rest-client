package com.aeontronix.restclient;

import com.aeontronix.restclient.auth.AuthenticationHandler;
import com.aeontronix.restclient.http.HTTPClient;
import com.aeontronix.restclient.http.HTTPRequest;
import com.aeontronix.restclient.http.HTTPResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class RESTClientTest {
    public static final String API_URL = "http://myapi";
    private HTTPClient httpClient;
    private RESTClient restClient;
    private HTTPRequest request;
    private HTTPResponse response;
    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);

    @BeforeEach
    public void init() {
        httpClient = Mockito.mock(HTTPClient.class);
        response = Mockito.mock(HTTPResponse.class);
        request = Mockito.mock(HTTPRequest.class);
        when(request.isRepeatable()).thenReturn(true);
        when(request.getURI()).thenReturn(URI.create("https://myapi"));
        final RequestParameters defaultRequestParameters = new RequestParameters();
        restClient = new RESTClient(httpClient, null, defaultRequestParameters);
        when(httpClient.createGetRequest()).thenReturn(request);
    }

    @Test
    public void testDefaultParamsPropagation() throws Exception {
        final RequestParameters defaultRequestParameters = new RequestParameters();
        defaultRequestParameters.setMaxRetries(5);
        restClient = new RESTClient(httpClient, null, defaultRequestParameters);
        final RESTRequest req = restClient.get(API_URL).build();
        assertEquals(5, req.getMaxRetries());
    }

    @Test
    public void testBearerRefresh() throws Exception {
        //noinspection ExtractMethodRecommender
        final RequestParameters defaultRequestParameters = new RequestParameters();
        defaultRequestParameters.setAuthenticationHandler(new AuthenticationHandler() {
            private boolean refreshed;

            @Override
            public boolean isRefreshRequired() {
                return false;
            }

            @Override
            public boolean isRefreshable() {
                return true;
            }

            @Override
            public void applyCredentials(RESTRequest request) {
            }

            @Override
            public void refreshCredential(RESTClient restClient) throws RESTException {
                refreshed = true;
            }
        });
        restClient = new RESTClient(httpClient, null, defaultRequestParameters);
        when(response.getStatusCode()).thenReturn(401, 200);
        when(response.hasContent()).thenReturn(false);
        when(response.isJson()).thenReturn(false);
        when(httpClient.execute(any(HTTPRequest.class), any(RequestParameters.class)))
                .thenReturn(this.response);
        try (RESTResponse response = restClient.get(API_URL).build().execute()) {
            assertEquals(200, response.getStatusCode());
        }
    }

    @Test
    public void testRetry() throws Exception {
        when(response.getStatusCode()).thenReturn(429, 200);
        when(response.hasContent()).thenReturn(true);
        when(response.isJson()).thenReturn(true);
        when(response.getContentStream()).thenReturn(new ByteArrayInputStream("{ \"message\":\"service down\"}".getBytes(StandardCharsets.UTF_8)),
                new ByteArrayInputStream("\"ok\"".getBytes(StandardCharsets.UTF_8)));
        when(response.getHeader("Content-Type")).thenReturn("application/json");
        when(httpClient.execute(any(HTTPRequest.class), any(RequestParameters.class))).thenReturn(response);
        final RESTResponse response = restClient.get("https://myapi").retry(2).execute();
        assertEquals("\"ok\"", response.toObject(String.class));
        Mockito.verify(this.response, times(2)).close();
        Mockito.verify(this.response, times(2)).getContentStream();
    }

    public void mockResponse(int statusCode, String content, boolean json) throws Exception {
        when(response.getStatusCode()).thenReturn(statusCode);
        mockResponse(content, json);
    }

    public void mockResponse(String content, boolean json) throws Exception {
        when(response.hasContent()).thenReturn(content != null);
        when(response.isJson()).thenReturn(content != null && json);
        if (content != null) {
            when(response.getContentStream()).thenReturn(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
            if (json) {
                when(response.getHeader("Content-Type")).thenReturn("application/json");
            }
        } else {
            when(response.getContentStream()).thenReturn(new ByteArrayInputStream(new byte[0]));
        }
        when(httpClient.execute(any(HTTPRequest.class), any(RequestParameters.class))).thenReturn(response);
    }

    private void verifyResponseClose(int times) throws IOException {
        Mockito.verify(this.response, times(times)).close();
    }
}

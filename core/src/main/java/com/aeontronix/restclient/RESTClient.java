package com.aeontronix.restclient;

import com.aeontronix.commons.exception.UnexpectedException;
import com.aeontronix.commons.io.IOUtils;
import com.aeontronix.restclient.http.HTTPClient;
import com.aeontronix.restclient.http.HTTPClientFactory;
import com.aeontronix.restclient.http.HTTPRequest;
import com.aeontronix.restclient.json.JsonConverterFactory;
import dev.failsafe.RetryPolicy;
import dev.failsafe.function.CheckedBiPredicate;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutorService;

import static org.slf4j.LoggerFactory.getLogger;

public class RESTClient implements Closeable {
    private static final Logger logger = getLogger(RESTClient.class);
    public static final int DEFAULT_MAX_CONN_TOTAL = 1000;
    public static final int DEFAULT_MAX_CONN_PER_ROUTE = 500;
    public static final int TIMEOUT = 5000;
    private final @NotNull HTTPClient httpClient;
    private final ExecutorService executorService;
    private final @NotNull RequestParameters defaultRequestParameters;
    private static final HTTPClientFactory defaultHttpClientFactory = findFactory(HTTPClientFactory.class);
    private static final JsonConverterFactory defaultJsonConverterFactory = findFactory(JsonConverterFactory.class);
    private final RetryPolicy<RESTResponse> defaultRetryPolicy;
    private final Duration defaultRetryDelay;
    private HashMap<String, LocalDateTime> globalDelays = new HashMap<>();
    private Integer defaultMaxRetries;

    private static <X> X findFactory(Class<X> factoryClass) {
        final Iterator<X> hcIt = ServiceLoader.load(factoryClass).iterator();
        if (hcIt.hasNext()) {
            final X factory = hcIt.next();
            if (!hcIt.hasNext()) {
                return factory;
            }
        }
        return null;
    }

    public RESTClient(@NotNull HTTPClient httpClient, ExecutorService executorService, @NotNull RequestParameters defaultRequestParameters) {
        this.httpClient = httpClient;
        this.executorService = executorService;
        this.defaultRequestParameters = defaultRequestParameters;
        defaultRetryDelay = defaultRequestParameters.getRetryDelay();
        defaultMaxRetries = defaultRequestParameters.getMaxRetries();
        if (defaultMaxRetries == null) {
            defaultMaxRetries = 5;
        }
        defaultRetryPolicy = createRetryPolicy(defaultMaxRetries, defaultRetryDelay);
    }

    private RetryPolicy<RESTResponse> createRetryPolicy(Integer maxRetries, Duration retryDelay) {
        return RetryPolicy.<RESTResponse>builder()
                .withBackoff(retryDelay != null ? retryDelay : Duration.ofSeconds(3), Duration.ofSeconds(30))
                .withMaxRetries(maxRetries)
                .abortIf((restResponse, throwable) -> !(throwable instanceof RESTException) || !((RESTException) throwable).isRetryable())
                .onRetryScheduled( l -> {
                    RESTException retryException = (RESTException) l.getLastException();
                    if (retryException.isDelayAllRequests()) {
                        RESTRequest request = retryException.getRequest();
                        LocalDateTime delayRetryUntil = retryException.getDelayRetryUntil();
                        request.getRestClient().setGlobalDelay(request.getHost(), delayRetryUntil);
                    }
                    retryException.getResponse().close();
                })
                .withDelayFnOn(context -> {
                    RESTException retryException = context.getLastException();
                    RESTRequest request = retryException.getRequest();
                    LocalDateTime delayRetryUntil = retryException.getDelayRetryUntil();
                    if (delayRetryUntil != null) {
//                        if (retryException.isDelayAllRequests()) {
//                            request.getRestClient().setGlobalDelay(request.getHost(), delayRetryUntil);
//                        }
                        Duration delayDuration = Duration.between(LocalDateTime.now(), delayRetryUntil);
                        if (delayDuration.isNegative()) {
                            return Duration.ZERO;
                        } else {
                            return delayDuration;
                        }
                    } else {
                        return Duration.ofMillis(-1);
                    }
                }, RESTException.class).build();
    }

    public RetryPolicy<RESTResponse> getRetryPolicy(Integer maxRetries, Duration retryDelay) {
        if (maxRetries == null && retryDelay == null) {
            return defaultRetryPolicy;
        } else {
            return createRetryPolicy(maxRetries, retryDelay);
        }
    }

    public LocalDateTime getGlobalDelay(String host) {
        return globalDelays.get(host);
    }

    public void setGlobalDelay(String host, LocalDateTime delayUntil) {
        globalDelays.put(host, delayUntil);
    }

    public void removeGlobalDelay(String host) {
        globalDelays.remove(host);
    }

    public HTTPClient getHttpClient() {
        return httpClient;
    }

    public RequestParameters getDefaultRequestParameters() {
        return defaultRequestParameters;
    }

    public RESTClientHostBuilder host(@NotNull URI hostUri) {
        return new RESTClientHostBuilder(this, hostUri);
    }

    public RESTClientHostBuilder host(@NotNull String hostUri) {
        return host(URI.create(hostUri));
    }

    public RESTClientRequestBuilder request(@NotNull HTTPRequest request) {
        return request(request, defaultRequestParameters);
    }

    RESTClientRequestBuilder request(@NotNull HTTPRequest request, RequestParameters requestParameters) {
        return new RESTClientRequestBuilder(this, request, requestParameters);
    }

    public RESTClientRequestBuilder request(HTTPRequest request, URI uri) {
        return request(request).uri(uri);
    }


    public RESTClientRequestBuilder request(String method) {
        return request(httpClient.createRequest(method), defaultRequestParameters);
    }

    public RESTClientRequestBuilder get() {
        return request(httpClient.createGetRequest(), defaultRequestParameters);
    }

    public RESTClientRequestBuilder get(URI uri) {
        return get().uri(uri);
    }

    public <X> X get(URI uri, Class<X> responseObjectClass) throws RESTException {
        return get(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> getList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return get(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder get(String uri) {
        return get().uri(uri);
    }

    public <X> X get(String uri, Class<X> responseObjectClass) throws RESTException {
        return get(uri).executeAndConvertToObject(responseObjectClass);
    }

    public RESTClientRequestBuilder delete() {
        return request(httpClient.createDeleteRequest());
    }

    public RESTClientRequestBuilder delete(URI uri) {
        return delete().uri(uri);
    }

    public RESTClientRequestBuilder delete(String uri) {
        return delete().uri(uri);
    }

    public <X> X delete(URI uri, Class<X> responseObjectClass) throws RESTException {
        return delete(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X delete(String uri, Class<X> responseObjectClass) throws RESTException {
        return delete(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> deleteList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return delete(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder post() {
        return request(httpClient.createPostRequest());
    }

    public RESTClientRequestBuilder post(URI uri) {
        return post().uri(uri);
    }

    public RESTClientRequestBuilder post(String uri) {
        return post().uri(uri);
    }

    public <X> X post(URI uri, Class<X> responseObjectClass) throws RESTException {
        return post(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X post(String uri, Class<X> responseObjectClass) throws RESTException {
        return post(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> postList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return post(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder put() {
        return request(httpClient.createPutRequest());
    }

    public RESTClientRequestBuilder put(URI uri) {
        return put().uri(uri);
    }

    public RESTClientRequestBuilder put(String uri) {
        return put().uri(uri);
    }

    public <X> X put(URI uri, Class<X> responseObjectClass) throws RESTException {
        return put(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X put(String uri, Class<X> responseObjectClass) throws RESTException {
        return put(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> putList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return put(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder patch() {
        return request(httpClient.createPatchRequest());
    }

    public RESTClientRequestBuilder patch(URI uri) {
        return patch().uri(uri);
    }

    public RESTClientRequestBuilder patch(String uri) {
        return patch().uri(uri);
    }

    public <X> X patch(URI uri, Class<X> responseObjectClass) throws RESTException {
        return patch(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X patch(String uri, Class<X> responseObjectClass) throws RESTException {
        return patch(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> patchList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return patch(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder head() {
        return request(httpClient.createHeadRequest());
    }

    public RESTClientRequestBuilder head(URI uri) {
        return head().uri(uri);
    }

    public RESTClientRequestBuilder head(String uri) {
        return head().uri(uri);
    }

    public <X> X head(URI uri, Class<X> responseObjectClass) throws RESTException {
        return head(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X head(String uri, Class<X> responseObjectClass) throws RESTException {
        return head(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> headList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return head(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder trace() {
        return request(httpClient.createTraceRequest());
    }

    public RESTClientRequestBuilder trace(URI uri) {
        return trace().uri(uri);
    }

    public RESTClientRequestBuilder trace(String uri) {
        return trace().uri(uri);
    }

    public <X> X trace(URI uri, Class<X> responseObjectClass) throws RESTException {
        return trace(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X trace(String uri, Class<X> responseObjectClass) throws RESTException {
        return trace(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> traceList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return trace(uri).executeAndConvertToList(responseObjectClass);
    }

    public RESTClientRequestBuilder options() {
        return request(httpClient.createOptionsRequest());
    }

    public RESTClientRequestBuilder options(URI uri) {
        return options().uri(uri);
    }

    public RESTClientRequestBuilder options(String uri) {
        return options().uri(uri);
    }

    public <X> X options(URI uri, Class<X> responseObjectClass) throws RESTException {
        return options(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> X options(String uri, Class<X> responseObjectClass) throws RESTException {
        return options(uri).executeAndConvertToObject(responseObjectClass);
    }

    public <X> List<X> optionsList(URI uri, Class<X> responseObjectClass) throws RESTException {
        return options(uri).executeAndConvertToList(responseObjectClass);
    }

    @Override
    public void close() throws IOException {
        IOUtils.close(httpClient);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends RequestParametersBuilder<Builder> {
        private boolean cookies;
        private ProxySettings proxySettings;
        private int maxConnTotal = DEFAULT_MAX_CONN_TOTAL;
        private int maxConnPerRoute = DEFAULT_MAX_CONN_PER_ROUTE;
        private ExecutorService executorService;
        private HTTPClient httpClient;
        private HTTPClientFactory httpClientFactory = defaultHttpClientFactory;
        private JsonConverterFactory jsonConverterFactory = defaultJsonConverterFactory;
        private boolean insecure;

        private Builder() {
        }

        public Builder httpClient(HTTPClient httpClient) {
            this.httpClient = httpClient;
            return this;
        }

        public Builder cookies(boolean cookies) {
            this.cookies = cookies;
            return this;
        }

        public Builder cookies() {
            return cookies(true);
        }

        public Builder noCookies() {
            return cookies(false);
        }

        public Builder maxConnTotal(int maxConnTotal) {
            this.maxConnTotal = maxConnTotal;
            return this;
        }

        public Builder maxConnPerRoute(int maxConnPerRoute) {
            this.maxConnPerRoute = maxConnPerRoute;
            return this;
        }

        public Builder proxy(URI proxyUri, String username, String password) {
            proxySettings = new ProxySettings(proxyUri, username, password, null);
            return this;
        }

        public Builder proxy(URI proxyUri, String username, String password, String... nonProxyHosts) {
            proxySettings = new ProxySettings(proxyUri, username, password, nonProxyHosts != null ? new HashSet<>(Arrays.asList(nonProxyHosts)) : null);
            return this;
        }

        public Builder proxy(ProxySettings proxySettings) {
            this.proxySettings = proxySettings;
            return this;
        }

        public Builder executorService(ExecutorService executorService) {
            this.executorService = executorService;
            return this;
        }

        public boolean isCookies() {
            return cookies;
        }

        public ProxySettings getProxySettings() {
            return proxySettings;
        }

        public int getMaxConnTotal() {
            return maxConnTotal;
        }

        public int getMaxConnPerRoute() {
            return maxConnPerRoute;
        }

        public boolean isInsecure() {
            return insecure;
        }

        public Builder insecure(boolean insecure) {
            this.insecure = insecure;
            return this;
        }

        public Builder insecure() {
            this.insecure = true;
            return this;
        }

        public Builder httpClientFactory(HTTPClientFactory httpClientFactory) {
            this.httpClientFactory = httpClientFactory;
            return this;
        }

        public Builder jsonConverterFactory(JsonConverterFactory jsonConverterFactory) {
            this.jsonConverterFactory = jsonConverterFactory;
            return this;
        }

        public RESTClient build() {
            if (httpClient == null) {
                if (httpClientFactory != null) {
                    httpClient = defaultHttpClientFactory.create(builder(), insecure);
                } else {
                    try {
                        httpClient = createDefaultInstance(HTTPClientFactory.class,
                                "com.aeontronix.restclient.http.apache.HTTPClientApacheFactory").create(builder(), insecure);
                    } catch (ClassNotFoundException e) {
                        throw new IllegalStateException("Unable to identify HttpClientFactory implementation, manual assignment required");

                    }
                }
            }
            if (requestParameters.getJsonConverter() == null) {
                if (jsonConverterFactory == null) {
                    try {
                        jsonConverterFactory = createDefaultInstance(JsonConverterFactory.class, "com.aeontronix.restclient.json.JsonConverterJacksonFactory");
                    } catch (ClassNotFoundException e) {
                        logger.debug("No json converter factory found");
                    }
                }
                if (jsonConverterFactory != null) {
                    requestParameters.setJsonConverter(jsonConverterFactory.build(requestParameters.isLaxJsonConverter(), requestParameters.getObjectMapperInjections()));
                }
            }
            if (requestParameters.getPaginationRequestTransformer() == null) {
                requestParameters.setPaginationRequestTransformer(new PaginationRequestTransformerDefaultImpl());
            }
            return new RESTClient(httpClient, executorService, requestParameters);
        }

        private static <X> X createDefaultInstance(Class<X> ifClass, String className) throws ClassNotFoundException {
            try {
                return ifClass.cast(Class.forName(className).newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                throw new UnexpectedException(e);
            }
        }
    }
}

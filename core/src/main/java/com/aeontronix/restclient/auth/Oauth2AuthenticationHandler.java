package com.aeontronix.restclient.auth;

import com.aeontronix.commons.exception.UnexpectedException;
import com.aeontronix.restclient.RESTClient;
import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;
import com.aeontronix.restclient.json.JsonConvertionException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class Oauth2AuthenticationHandler extends BearerTokenAuthenticationHandler {
    private String tokenURL;
    private String clientId;
    private String clientSecret;
    private String grantType;
    private Map<String, String> parameters = new HashMap<>();
    private RESTClient oauthRestClient;
    private LocalDateTime expires;

    public Oauth2AuthenticationHandler(String tokenURL, String clientId, String clientSecret, String grantType, Map<String, String> parameters) {
        this.tokenURL = tokenURL;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
        this.parameters = parameters;
        oauthRestClient = RESTClient.builder().build();
    }

    public Oauth2AuthenticationHandler(String tokenURL, String clientId, String clientSecret, Map<String, String> parameters) {
        this(tokenURL, clientId, clientSecret, "client_credentials", parameters);
    }

    public Oauth2AuthenticationHandler(String tokenURL, String clientId, String clientSecret) {
        this(tokenURL, clientId, clientSecret, null);
    }

    @Override
    public boolean isRefreshRequired() {
        return authzHeader == null || ( expires == null || LocalDateTime.now().isAfter(expires) );
    }

    @Override
    public boolean isRefreshable() {
        return true;
    }

    @Override
    public void refreshCredential(RESTClient restClient) throws RESTException, IOException {
        Map<String, String> request = new HashMap<>();
        request.put("client_id",clientId);
        request.put("client_secret",clientSecret);
        request.put("grant_type",grantType);
        if( parameters != null ) {
            request.putAll(parameters);
        }
        try {
            try (RESTResponse response = oauthRestClient.post(tokenURL).jsonBody(request).build().execute()) {
                Map resp = response.toObject(Map.class);
                Integer expiresIn = (Integer) resp.get("expires_in");
                if (expiresIn != null) {
                    expires = LocalDateTime.now().plusSeconds(expiresIn);
                }
                setBearerToken((String) resp.get("access_token"));
            }
        } catch (JsonConvertionException e) {
            throw new UnexpectedException(e);
        }
    }
}

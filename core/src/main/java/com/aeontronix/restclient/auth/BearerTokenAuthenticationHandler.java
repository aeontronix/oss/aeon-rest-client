package com.aeontronix.restclient.auth;

import com.aeontronix.restclient.RESTClient;
import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTRequest;

import java.io.IOException;

public class BearerTokenAuthenticationHandler implements AuthenticationHandler {
    protected String authzHeader;

    public BearerTokenAuthenticationHandler() {
    }

    public BearerTokenAuthenticationHandler(String bearerToken) {
        setBearerToken(bearerToken);
    }

    public void setBearerToken(String authzHeader) {
        this.authzHeader = "Bearer "+ authzHeader;
    }

    @Override
    public boolean isRefreshRequired() {
        return false;
    }

    @Override
    public boolean isRefreshable() {
        return false;
    }

    @Override
    public void applyCredentials(RESTRequest request) {
        request.setHeader("Authorization",authzHeader);
    }

    @Override
    public void refreshCredential(RESTClient restClient) throws RESTException, IOException {

    }
}

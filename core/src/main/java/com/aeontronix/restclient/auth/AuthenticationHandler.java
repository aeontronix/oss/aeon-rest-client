package com.aeontronix.restclient.auth;

import com.aeontronix.restclient.RESTClient;
import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTRequest;

import java.io.IOException;

public interface AuthenticationHandler {
    boolean isRefreshRequired();

    boolean isRefreshable();

    void applyCredentials(RESTRequest request);

    void refreshCredential(RESTClient restClient) throws RESTException, IOException;
}

package com.aeontronix.restclient.http;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

public interface HTTPRequest {
    URI getURI();

    void setURI(URI uri);

    boolean isBodyAllowed();

    void setBody(InputStream is);

    void setBody(byte[] data);

    boolean hasBody();

    @Nullable InputStream getContent() throws IOException;

    long getContentSize();

    void setHeader(String key, String value);

    @NotNull Map<String, List<String>> getAllHeaders();

    boolean isRepeatable();

    String getMethod();
}

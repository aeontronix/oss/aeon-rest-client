package com.aeontronix.restclient.http;

import com.aeontronix.restclient.RequestParameters;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface HTTPClient {
    HTTPResponse execute(HTTPRequest request, RequestParameters requestParameters) throws IOException;

    HTTPRequest createGetRequest();

    HTTPRequest createPostRequest();

    HTTPRequest createPutRequest();

    HTTPRequest createHeadRequest();

    HTTPRequest createDeleteRequest();

    HTTPRequest createPatchRequest();

    HTTPRequest createOptionsRequest();

    HTTPRequest createTraceRequest();

    @NotNull HTTPRequest createRequest(@NotNull String method);
}

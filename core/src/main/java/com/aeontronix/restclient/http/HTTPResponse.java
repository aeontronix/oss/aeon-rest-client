package com.aeontronix.restclient.http;

import com.aeontronix.restclient.RESTException;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface HTTPResponse extends Closeable {
    int getStatusCode();

    String getStatusReasons();

    String getStatusProtocolVersion();

    boolean hasContent();

    InputStream getContentStream() throws RESTException;

    long getContentLength();

    String getContentEncoding();

    String getHeader(String name);

    List<String> getHeaders(String name);

    Map<String, List<String>> getAllHeaders();

    boolean isJson();

    String getContentMimeType();
}

package com.aeontronix.restclient.http;

import com.aeontronix.restclient.RESTClient;

public interface HTTPClientFactory {
    HTTPClient create(RESTClient.Builder builder, boolean insecure);
}

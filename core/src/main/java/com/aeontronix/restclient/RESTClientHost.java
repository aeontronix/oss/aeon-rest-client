package com.aeontronix.restclient;

import com.aeontronix.commons.URLBuilder;
import com.aeontronix.restclient.http.HTTPClient;
import com.aeontronix.restclient.http.HTTPRequest;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.List;

public class RESTClientHost {
    private final RESTClient client;
    private final URI host;
    private final HTTPClient httpClient;
    private final RequestParameters requestParameters;

    public RESTClientHost(@NotNull RESTClient client, @NotNull URI host, RequestParameters requestParameters) {
        this.client = client;
        this.host = host;
        this.requestParameters = new RequestParameters(requestParameters);
        httpClient = client.getHttpClient();
    }

    RESTClientHostPathBuilder request(HTTPRequest request) {
        final RESTClientRequestBuilder requestBuilder = client.request(request, requestParameters);
        return new RESTClientHostPathBuilder(requestBuilder, new URLBuilder(host));
    }

    public RESTClientHostPathBuilder get() {
        return request(httpClient.createGetRequest());
    }

    public RESTClientRequestBuilder get(String path) {
        return get().path(path).build();
    }

    public <X> X get(String path, Class<X> classType) throws RESTException {
        return get(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> getList(String path, Class<X> classType) throws RESTException {
        return get(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder delete() {
        return request(httpClient.createDeleteRequest());
    }

    public RESTClientRequestBuilder delete(String path) {
        return delete().path(path).build();
    }

    public <X> X delete(String path, Class<X> classType) throws RESTException {
        return delete(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> deleteList(String path, Class<X> classType) throws RESTException {
        return delete(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder post() {
        return request(httpClient.createPostRequest());
    }

    public RESTClientRequestBuilder post(String path) {
        return post().path(path).build();
    }

    public <X> X post(String path, Class<X> classType) throws RESTException {
        return post(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> postList(String path, Class<X> classType) throws RESTException {
        return post(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder put() {
        return request(httpClient.createPutRequest());
    }

    public RESTClientRequestBuilder put(String path) {
        return put().path(path).build();
    }

    public <X> X put(String path, Class<X> classType) throws RESTException {
        return put(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> putList(String path, Class<X> classType) throws RESTException {
        return put(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder patch() {
        return request(httpClient.createPatchRequest());
    }

    public RESTClientRequestBuilder patch(String path) {
        return patch().path(path).build();
    }

    public <X> X patch(String path, Class<X> classType) throws RESTException {
        return patch(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> patchList(String path, Class<X> classType) throws RESTException {
        return patch(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder head() {
        return request(httpClient.createHeadRequest());
    }

    public RESTClientRequestBuilder head(String path) {
        return head().path(path).build();
    }

    public <X> X head(String path, Class<X> classType) throws RESTException {
        return head(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> headList(String path, Class<X> classType) throws RESTException {
        return head(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder trace() {
        return request(httpClient.createTraceRequest());
    }

    public RESTClientRequestBuilder trace(String path) {
        return trace().path(path).build();
    }

    public <X> X trace(String path, Class<X> classType) throws RESTException {
        return trace(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> traceList(String path, Class<X> classType) throws RESTException {
        return trace(path).executeAndConvertToList(classType);
    }

    public RESTClientHostPathBuilder options() {
        return request(httpClient.createOptionsRequest());
    }

    public RESTClientRequestBuilder options(String path) {
        return options().path(path).build();
    }

    public <X> X options(String path, Class<X> classType) throws RESTException {
        return options(path).executeAndConvertToObject(classType);
    }

    public <X> List<X> optionsList(String path, Class<X> classType) throws RESTException {
        return options(path).executeAndConvertToList(classType);
    }
}

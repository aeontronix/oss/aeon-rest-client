package com.aeontronix.restclient;

import java.util.List;

public class ResultsPage<X> {
    private final List<X> list;

    public ResultsPage(List<X> list) {
        this.list = list;
    }

    public List<X> getList() {
        return list;
    }
}

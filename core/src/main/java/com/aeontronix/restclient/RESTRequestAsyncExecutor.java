package com.aeontronix.restclient;

import java.util.concurrent.CompletableFuture;

public interface RESTRequestAsyncExecutor {
    CompletableFuture<RESTResponse> execute();
}

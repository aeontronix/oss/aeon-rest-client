package com.aeontronix.restclient;

public interface RESTRequestScheduler {
    RESTResponse executeNow(RESTRequest restRequest, RESTRequestExecutor requestExecutor) throws RESTException;
}

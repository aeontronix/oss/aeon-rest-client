package com.aeontronix.restclient;

import com.aeontronix.commons.io.IOUtils;
import com.aeontronix.restclient.http.HTTPResponse;
import org.slf4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import static com.aeontronix.restclient.RESTRequest.addHeadersToLog;
import static org.slf4j.LoggerFactory.getLogger;

public class RESTResponse implements AutoCloseable {
    private static final long DEBUG_MAX_CONTENT_SIZE = 500000L;
    private static final Logger logger = getLogger(RESTResponse.class);
    private final HTTPResponse httpResponse;
    private final RESTRequest request;
    private boolean credentialRefreshAttempted;
    private boolean closed;
    private byte[] content;

    public RESTResponse(HTTPResponse response, RESTRequest request) {
        this.httpResponse = response;
        this.request = request;
    }

    public RESTResponse(HTTPResponse response, RESTRequest request, boolean credentialRefreshAttempted) {
        this.httpResponse = response;
        this.request = request;
        this.credentialRefreshAttempted = credentialRefreshAttempted;
    }

    @Override
    public void close() throws IOException {
        IOUtils.close(httpResponse);
        closed = true;
    }

    public RESTRequest getRequest() {
        return request;
    }

    public int getStatusCode() {
        return httpResponse.getStatusCode();
    }

    public String getStatusReasons() {
        return httpResponse.getStatusReasons();
    }

    public String getStatusProtocolVersion() {
        return httpResponse.getStatusProtocolVersion();
    }

    public InputStream getContentStream() throws RESTException {
        if (httpResponse.hasContent()) {
            return new FilterInputStream(getContentStreamInternal()) {
                @Override
                public void close() throws IOException {
                    RESTResponse.this.close();
                }
            };
        } else {
            return null;
        }
    }

    public <X> List<X> toList(Class<X> objectClass) throws RESTException, ResponseConversionException {
        return toList(objectClass, null);
    }

    @SuppressWarnings("unchecked")
    public <X> List<X> toList(Class<X> objectClass, String dataPath) throws RESTException, ResponseConversionException {
        return (List<X>) toObject(ResponseType.list(objectClass), dataPath);
    }

    public <X> X toObject(Class<X> objectClass) throws RESTException, ResponseConversionException {
        return toObject(objectClass, null);
    }

    public <X> X toObject(Class<X> objectClass, String dataPath) throws RESTException, ResponseConversionException {
        return toObject(ResponseType.object(objectClass), dataPath);
    }

    public <X> X toObject(ResponseType<X> responseType, String dataPath) throws RESTException, ResponseConversionException {
        if (httpResponse.hasContent()) {
            try (final InputStream content = getContentStreamInternal()) {
                if (httpResponse.isJson() && request.getJsonConverter() != null) {
                    return request.getJsonConverter().convertFromJson(responseType, content, dataPath);
                } else if (responseType.isString()) {
                    if (dataPath != null) {
                        throw new IllegalArgumentException("data path not supported for string payloads");
                    }
                    return responseType.getClassType().cast(IOUtils.toString(content));
                } else if (responseType.isByteArray()) {
                    if (dataPath != null) {
                        throw new IllegalArgumentException("data path not supported for byte array payloads");
                    }
                    return responseType.getClassType().cast(IOUtils.toByteArray(content));
                } else {
                    throw new ResponseConversionException("Conversion of " + httpResponse.getContentMimeType() + " response to " + responseType + " not supported");
                }
            } catch (IOException e) {
                throw new RESTException(e.getMessage(), e);
            } finally {
                IOUtils.close(this);
            }
        } else {
            return null;
        }
    }

    private InputStream getContentStreamInternal() throws RESTException {
        return content != null ? new ByteArrayInputStream(content) : httpResponse.getContentStream();
    }

    public <X> PaginatedResponse<X> paginatedResults(Class<X> objectClass) throws RESTException, ResponseConversionException {
        return new PaginatedResponse<X>(this, objectClass);
    }

    public boolean isCredentialRefreshAttempted() {
        return credentialRefreshAttempted;
    }

    public boolean hasContent() {
        return httpResponse.hasContent();
    }

    public long getContentLength() {
        return httpResponse.getContentLength();
    }

    public String getContentEncoding() {
        return httpResponse.getContentEncoding();
    }

    public String getHeader(String name) {
        return httpResponse.getHeader(name);
    }

    public List<String> getHeaders(String name) {
        return httpResponse.getHeaders(name);
    }

    public Map<String, List<String>> getAllHeaders() {
        return httpResponse.getAllHeaders();
    }

    public boolean isJson() {
        return httpResponse.isJson();
    }

    public String getContentMimeType() {
        return httpResponse.getContentMimeType();
    }

    /**
     * Retrieve the retry after value
     *
     * @return
     */
    public LocalDateTime getRetryAfter() {
        final String retryAfter = getHeader("Retry-After");
        if (retryAfter == null) {
            return null;
        }
        try {
            long delayValue = Long.parseLong(retryAfter);
            return LocalDateTime.now().plus(delayValue, ChronoUnit.SECONDS);
        } catch (NumberFormatException e) {
            try {
                return ZonedDateTime.parse(retryAfter, DateTimeFormatter.RFC_1123_DATE_TIME).toLocalDateTime();
            } catch (DateTimeParseException ex) {
                logger.debug("Invalid Retry-After found, ignoring: {}", retryAfter);
            }
        }
        return null;
    }

    public boolean isClosed() {
        return closed;
    }

    public boolean isStatusCode401() {
        return getStatusCode() == 401;
    }

    byte[] peekContent() throws IOException, RESTException {
        if( content == null && httpResponse.hasContent() ) {
            content = IOUtils.toByteArray(httpResponse.getContentStream());
        }
        return content;
    }

    public String toLogString() throws RESTException, IOException {
        StringBuilder respLog = new StringBuilder("Received response status code ").append(getStatusCode())
                .append(" reason: ").append(getStatusReasons()).append("\n");
        Map<String, List<String>> headers = getAllHeaders();
        addHeadersToLog(respLog, headers);
        long responseSize = getContentLength();
        if (responseSize < 0 || responseSize > DEBUG_MAX_CONTENT_SIZE) {
            respLog.append("--- Unable to log response payload, size = ");
            respLog.append(responseSize);
            respLog.append(" ---\n");
        } else {
            byte[] content = peekContent();
            if (content != null) {
                respLog.append("---Content---\n").append(new String(content));
            }
        }
        return respLog.toString();
    }
}

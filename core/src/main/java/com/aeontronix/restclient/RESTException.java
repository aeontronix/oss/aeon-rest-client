/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.LocalDateTime;

import static com.aeontronix.restclient.errorhandling.RESTResponseValidatorDefaultImpl.findErrorMessage;

public class RESTException extends IOException {
    protected Integer statusCode;
    protected String reason;
    protected RESTRequest request;
    protected RESTResponse response;
    protected boolean retryable;
    protected LocalDateTime delayRetryUntil;
    protected boolean delayAllRequests;

    public RESTException(Throwable cause) {
        super(cause);
    }

    public RESTException(String message) {
        super(message);
    }

    public RESTException(String message, String reason, Integer statusCode) {
        super(message);
        this.reason = reason;
        this.statusCode = statusCode;
    }

    public RESTException(String message, Throwable cause) {
        super(message, cause);
    }

    public RESTException(String message, @NotNull RESTResponse response) {
        super(message);
        this.statusCode = response.getStatusCode();
        this.reason = response.getStatusReasons();
        this.response = response;
        this.request = response.getRequest();
    }

    public RESTException(@NotNull RESTResponse response) {
        super("Server returned status code " + response.getStatusCode() + " : " + findErrorMessage(response));
        this.statusCode = response.getStatusCode();
        this.reason = response.getStatusReasons();
        this.response = response;
        this.request = response.getRequest();
    }

    public RESTException(Integer statusCode, String reason, RESTRequest request, RESTResponse response) {
        this.statusCode = statusCode;
        this.reason = reason;
        this.request = request;
        this.response = response;
    }

    /**
     * If this constructor is used it will set retryable flag to true and assign retryDelay and globalRetry
     *
     * @param response    Response
     * @param delayRetryUntil  Retry delay in milliseconds (or null to calculate delay dynamically)
     * @param globalRetry if this is a global retry
     */
    public RESTException(RESTResponse response, LocalDateTime delayRetryUntil, boolean globalRetry) {
        this(response);
        this.retryable = true;
        this.delayRetryUntil = delayRetryUntil;
        this.delayAllRequests = globalRetry;
    }

    public RESTRequest getRequest() {
        return request;
    }

    public RESTResponse getResponse() {
        return response;
    }

    public int getStatusCode() {
        return statusCode != null ? statusCode : 0;
    }

    public boolean isStatusCode(int statusCode) {
        return this.statusCode != null && this.statusCode == statusCode;
    }

    public boolean isStatusCode(int from, int to) {
        return this.statusCode != null && this.statusCode >= from && this.statusCode <= to;
    }

    public String getReason() {
        return reason;
    }

    public boolean isRetryable() {
        return retryable;
    }

    public LocalDateTime getDelayRetryUntil() {
        return delayRetryUntil;
    }

    public boolean isDelayAllRequests() {
        return delayAllRequests;
    }

    public boolean isStatusCode401() {
        return getStatusCode() == 401;
    }
}

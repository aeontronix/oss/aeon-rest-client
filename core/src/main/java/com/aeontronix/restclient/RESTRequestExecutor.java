package com.aeontronix.restclient;

import java.util.concurrent.CompletableFuture;

public interface RESTRequestExecutor {
    RESTResponse execute() throws RESTException;
}

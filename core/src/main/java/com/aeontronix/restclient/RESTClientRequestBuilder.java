package com.aeontronix.restclient;

import com.aeontronix.restclient.http.HTTPRequest;
import com.aeontronix.restclient.json.JsonConvertionException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

public class RESTClientRequestBuilder extends RequestParametersBuilder<RESTClientRequestBuilder> {
    private final RESTClient restClient;
    private final HTTPRequest request;

    RESTClientRequestBuilder(RESTClient restClient, HTTPRequest request, RequestParameters requestParameters) {
        super(requestParameters);
        this.restClient = restClient;
        this.request = request;
    }

    public RESTClientRequestBuilder uri(URI uri) {
        request.setURI(uri);
        return this;
    }

    public RESTClientRequestBuilder uri(String uri) {
        return uri(URI.create(uri));
    }

    public RESTClientRequestBuilder conditionalHeader(boolean condition, String key, String value) {
        if (condition) {
            header(key, value);
        }
        return this;
    }

    public RESTClientRequestBuilder header(String key, String value) {
        request.setHeader(key, value);
        return this;
    }

    public RESTClientRequestBuilder entity(byte[] data) {
        entityPreCheck();
        request.setBody(data);
        return this;
    }

    public RESTClientRequestBuilder entity(InputStream is) {
        entityPreCheck();
        request.setBody(is);
        return this;
    }

    public void entityPreCheck() {
        if (!request.isBodyAllowed()) {
            throw new IllegalStateException("Request is not an entity enclosing request");
        }
    }

    public RESTClientRequestBuilder jsonBody(Object object) throws JsonConvertionException {
        entity(requestParameters.getJsonConverter().convertToJson(object));
        request.setHeader("Content-Type", "application/json");
        return this;
    }

    public RESTRequest build() {
        return new RESTRequest(restClient, request, this.requestParameters);
    }

    public RESTResponse execute() throws RESTException {
        return build().execute();
    }

    /**
     * Retrieves paginated results
     *
     * @return Paginated list of objects
     * @throws RESTException If an HTTP error occurs
     */
    @SuppressWarnings("resource")
    public <X> PaginatedResponse<X> executePaginated(@NotNull Class<X> classType) throws RESTException {
        return paginate().build().execute().paginatedResults(classType);
    }

    @SuppressWarnings("resource")
    public <X> PaginatedResponse<X> executePaginated(@NotNull Class<X> classType, @NotNull String offsetParam,
                                                     @NotNull String limitParam, @NotNull String valuesPath) throws RESTException {
        return paginate().pageValuesPath(valuesPath)
                .paginationRequestTransformer(new PaginationRequestTransformerDefaultImpl(offsetParam, limitParam))
                .build().execute().paginatedResults(classType);
    }

    public <X> Object executeAndConvert(ResponseType<?> responseType) throws RESTException, ResponseConversionException {
        return executeAndConvert(responseType, null);
    }

    public <X> Object executeAndConvert(ResponseType<?> responseType, String dataPath) throws RESTException, ResponseConversionException {
        try {
            try (final RESTResponse response = execute()) {
                return response.toObject(responseType, dataPath);
            }
        } catch (IOException e) {
            throw new RESTException(e);
        }
    }

    public <X> X executeAndConvertToObject(Class<X> objectType) throws RESTException, ResponseConversionException {
        return objectType.cast(executeAndConvert(ResponseType.object(objectType)));
    }

    @SuppressWarnings("unchecked")
    public <X> List<X> executeAndConvertToList(Class<X> objectType) throws RESTException, ResponseConversionException {
        return (List<X>) executeAndConvert(ResponseType.list(objectType));
    }

    @SuppressWarnings("unchecked")
    public <X> List<X> executeAndConvertToList(Class<X> objectType, String dataPath) throws RESTException, ResponseConversionException {
        return (List<X>) executeAndConvert(ResponseType.list(objectType),dataPath);
    }
}

package com.aeontronix.restclient;

public interface RequestTransformer {
    RESTRequest transform(RESTRequest request);
}

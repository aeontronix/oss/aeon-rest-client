package com.aeontronix.restclient;

import com.aeontronix.commons.URLBuilder;

public class RESTClientHostPathBuilder {
    private RESTClientRequestBuilder requestBuilder;
    private URLBuilder urlBuilder;

    RESTClientHostPathBuilder(RESTClientRequestBuilder requestBuilder, URLBuilder urlBuilder) {
        this.requestBuilder = requestBuilder;
        this.urlBuilder = urlBuilder;
    }

    private RESTClientHostPathBuilder(URLBuilder urlBuilder) {
        this.urlBuilder = urlBuilder;
    }

    public RESTClientRequestBuilder build() {
        return requestBuilder.uri(urlBuilder.toURI());
    }

    public RESTClientHostPathBuilder path(String path) {
        urlBuilder = urlBuilder.path(path);
        return this;
    }

    public RESTClientHostPathBuilder fragment(String fragment) {
        urlBuilder = urlBuilder.fragment(fragment);
        return this;
    }

    public RESTClientHostPathBuilder pathEl(String... pathElements) {
        urlBuilder = urlBuilder.pathEl(pathElements);
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, String value) {
        urlBuilder = urlBuilder.queryParam(key, value);
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, String value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, long value) {
        urlBuilder = urlBuilder.queryParam(key, value);
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, Long value) {
        if (value != null) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, long value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, Long value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, int value) {
        urlBuilder = urlBuilder.queryParam(key, value);
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, int value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, Integer value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, Integer value) {
        if (value != null) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, byte value) {
        urlBuilder = urlBuilder.queryParam(key, value);
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, byte value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, Byte value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, Byte value) {
        if (value != null) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, boolean value) {
        urlBuilder = urlBuilder.queryParam(key, value);
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, boolean value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, Boolean value) {
        if (condition) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, Boolean value) {
        if (value != null) {
            queryParam(key, value);
        }
        return this;
    }

    public RESTClientHostPathBuilder queryParam(String key, String value, boolean encode, boolean append) {
        urlBuilder = urlBuilder.queryParam(key, value, encode, append);
        return this;
    }

    public RESTClientHostPathBuilder conditionalQueryParam(boolean condition, String key, String value, boolean encode, boolean append) {
        if (condition) {
            queryParam(key, value, encode, append);
        }
        return this;
    }

    public RESTClientHostPathBuilder setUserInfo(String userInfo) {
        urlBuilder = urlBuilder.setUserInfo(userInfo);
        return this;
    }

    public RESTClientHostPathBuilder setProtocol(String protocol) {
        urlBuilder = urlBuilder.setProtocol(protocol);
        return this;
    }

    public RESTClientHostPathBuilder setRef(String ref) {
        urlBuilder = urlBuilder.setRef(ref);
        return this;
    }

    public RESTClientHostPathBuilder setHost(String host) {
        urlBuilder = urlBuilder.setHost(host);
        return this;
    }

    public RESTClientHostPathBuilder setPort(int port) {
        urlBuilder = urlBuilder.setPort(port);
        return this;
    }
}

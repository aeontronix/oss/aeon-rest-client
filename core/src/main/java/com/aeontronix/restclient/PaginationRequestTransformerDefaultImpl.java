package com.aeontronix.restclient;

import com.aeontronix.commons.URLBuilder;

public class PaginationRequestTransformerDefaultImpl implements PaginationRequestTransformer {
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    private final String offsetQueryParameterName;
    private final String pageSizeQueryParameterName;

    public PaginationRequestTransformerDefaultImpl() {
        this(OFFSET, LIMIT);
    }

    public PaginationRequestTransformerDefaultImpl(String offsetQueryParameterName, String limitQueryParameterName) {
        this.offsetQueryParameterName = offsetQueryParameterName;
        this.pageSizeQueryParameterName = limitQueryParameterName;
    }

    @Override
    public RESTRequest transform(RESTRequest request) {
        request.setURI(new URLBuilder(request.getURI())
                .queryParam(offsetQueryParameterName, request.getPageOffset())
                .queryParam(pageSizeQueryParameterName, request.getPageSize()).toURI());
        return request;
    }
}

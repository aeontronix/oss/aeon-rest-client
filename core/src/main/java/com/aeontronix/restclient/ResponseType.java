package com.aeontronix.restclient;

public class ResponseType<X> {
    public static final ResponseType<? extends Object> OBJECT = object(Object.class);
    private Type type;
    public Class<X> classType;

    private ResponseType(Type type, Class<X> cl) {
        this.type = type;
        this.classType = cl;
    }

    public Type getType() {
        return type;
    }

    public Class<X> getClassType() {
        return classType;
    }

    public static <X> ResponseType<X> object(Class<X> cl) {
        return new ResponseType<X>(Type.OBJECT, cl);
    }

    public static <X> ResponseType<X> list(Class<X> cl) {
        return new ResponseType<X>(Type.LIST, cl);
    }

    public boolean isString() {
        return String.class.isAssignableFrom(classType);
    }

    public boolean isByteArray() {
        return byte[].class.isAssignableFrom(classType);
    }

    @Override
    public String toString() {
        return type+" : "+ classType.getName();
    }

    public enum Type {
        OBJECT, LIST
    }
}

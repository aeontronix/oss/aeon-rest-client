package com.aeontronix.restclient;

import com.aeontronix.restclient.auth.AuthenticationHandler;
import com.aeontronix.restclient.errorhandling.RESTResponseValidator;
import com.aeontronix.restclient.json.JsonConverter;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.util.Map;

@SuppressWarnings({"unchecked", "rawtypes"})
public abstract class RequestParametersBuilder<X extends RequestParametersBuilder> {
    protected RequestParameters requestParameters;

    protected RequestParametersBuilder() {
        requestParameters = new RequestParameters();
    }

    protected RequestParametersBuilder(RequestParameters requestParameters) {
        this.requestParameters = new RequestParameters(requestParameters);
    }

    public RequestParameters getRequestParameters() {
        return requestParameters;
    }

    public X authenticationHandler(AuthenticationHandler authenticationHandler) {
        requestParameters.setAuthenticationHandler(authenticationHandler);
        return (X) this;
    }

    public X jsonConverter(JsonConverter jsonConverter) {
        requestParameters.setJsonConverter(jsonConverter);
        return (X) this;
    }

    public X retry(Integer maxRetries) {
        requestParameters.setMaxRetries(maxRetries);
        return (X) this;
    }

    public X responseTransformer(Transformer responseTransformer) {
        requestParameters.setResponseTransformer(responseTransformer);
        return (X) this;
    }

    public X responseValidator(RESTResponseValidator responseValidator) {
        requestParameters.setResponseValidator(responseValidator);
        return (X) this;
    }

    public X maxRetries(Integer maxRetries) {
        requestParameters.setMaxRetries(maxRetries);
        return (X) this;
    }

    public X retryDelay(Duration retryDelay) {
        requestParameters.setRetryDelay(retryDelay);
        return (X) this;
    }

    public X pageSize(int pageSize) {
        requestParameters.setPageSize(pageSize);
        return (X) this;
    }

    public X pageOffsetQueryParameterName(String pageOffsetQueryParameterName) {
        requestParameters.setPageOffsetQueryParameterName(pageOffsetQueryParameterName);
        return (X) this;
    }

    public X pageSizeQueryParameterName(String pageSizeQueryParameterName) {
        requestParameters.setPageSizeQueryParameterName(pageSizeQueryParameterName);
        return (X) this;
    }

    public X paginationRequestTransformer(PaginationRequestTransformer paginationRequestTransformer) {
        requestParameters.setPaginationRequestTransformer(paginationRequestTransformer);
        return (X) this;
    }

    public X paginate(boolean paginate) {
        requestParameters.setPaginated(paginate);
        return (X) this;
    }

    public X paginate() {
        return paginate(true);
    }

    public X pageValuesPath(String pageValuesPath) {
        requestParameters.setPageValuesPath(pageValuesPath);
        return (X) this;
    }

    @NotNull
    public X requestScheduler(@NotNull RESTRequestScheduler requestScheduler) {
        requestParameters.setRequestScheduler(requestScheduler);
        return (X) this;
    }

    @NotNull
    public X connectionTimeout(Integer connectionTimeout) {
        requestParameters.setConnectionTimeout(connectionTimeout);
        return (X) this;
    }

    @NotNull
    public X socketTimeout(Integer socketTimeout) {
        requestParameters.setSocketTimeout(socketTimeout);
        return (X) this;
    }

    @NotNull
    public X objectMapperInjections(@NotNull Map<Object, Object> objectMapperInjections) {
        requestParameters.setObjectMapperInjections(objectMapperInjections);
        return (X) this;
    }

    @NotNull
    public X connectionPoolTimeout(Integer connectionPoolTimeout) {
        requestParameters.setConnectionPoolTimeout(connectionPoolTimeout);
        return (X) this;
    }
}

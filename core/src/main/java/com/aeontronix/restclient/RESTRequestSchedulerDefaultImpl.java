package com.aeontronix.restclient;

import com.aeontronix.commons.ThreadUtils;
import com.aeontronix.commons.exception.UnexpectedException;
import dev.failsafe.Failsafe;
import dev.failsafe.FailsafeException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class RESTRequestSchedulerDefaultImpl implements RESTRequestScheduler {
    private final ScheduledExecutorService scheduledExecutorService;

    public RESTRequestSchedulerDefaultImpl() {
        this(Executors.newSingleThreadScheduledExecutor());
    }

    public RESTRequestSchedulerDefaultImpl(ScheduledExecutorService scheduledExecutorService) {
        this.scheduledExecutorService = scheduledExecutorService;
    }

    @Override
    public RESTResponse executeNow(RESTRequest restRequest, RESTRequestExecutor requestExecutor) throws RESTException {
        final String host = restRequest.getHost();
        RESTClient restClient = restRequest.getRestClient();
        try {
            final LocalDateTime globalDelay = restClient.getGlobalDelay(host);
            if (globalDelay != null) {
                if (globalDelay.isAfter(LocalDateTime.now())) {
                    sleepUntil(globalDelay);
                } else {
                    restClient.removeGlobalDelay(host);
                }
            }
            return Failsafe.with(restRequest.getRestClient().getRetryPolicy(restRequest.getMaxRetries(),
                    restRequest.getRetryDelay())).get(requestExecutor::execute);
        } catch (FailsafeException failsafeException) {
            if (failsafeException.getCause() instanceof RESTException) {
                RESTException restException = (RESTException) failsafeException.getCause();
                final LocalDateTime retryDelay = restException.getDelayRetryUntil();
                if (retryDelay != null && restException.isDelayAllRequests()) {
                    restClient.setGlobalDelay(host, retryDelay);
                }
                throw restException;
            } else {
                throw new UnexpectedException(failsafeException.getCause());
            }
        }
    }

    private static void sleepUntil(LocalDateTime delayUntil) {
        final long sleepDuration = Duration.between(LocalDateTime.now(), delayUntil).toMillis();
        if (sleepDuration > 0) {
            ThreadUtils.sleep(sleepDuration);
        }
    }
}

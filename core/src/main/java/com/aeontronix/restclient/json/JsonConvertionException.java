/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient.json;

public class JsonConvertionException extends Exception {
    public JsonConvertionException() {
    }

    public JsonConvertionException(String message) {
        super(message);
    }

    public JsonConvertionException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonConvertionException(Throwable cause) {
        super(cause);
    }

    public JsonConvertionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

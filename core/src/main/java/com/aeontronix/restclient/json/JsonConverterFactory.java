/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient.json;

import java.util.Map;

public interface JsonConverterFactory {
    JsonConverter build(boolean lax, Map<Object, Object> objectMapperInjections);
}

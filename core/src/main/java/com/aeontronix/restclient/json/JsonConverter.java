/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient.json;

import com.aeontronix.restclient.*;

import java.io.InputStream;

public interface JsonConverter {
    byte[] convertToJson(Object object) throws JsonConvertionException;
    String convertToJsonString(Object object) throws JsonConvertionException;

    <X> X convertFromJson(ResponseType<X> responseType, InputStream is, String jsonPointerPath) throws RESTException, ResponseConversionException;

    <X> ResultsPage toPage(Class<X> classType, RESTResponse response) throws RESTException, ResponseConversionException;
}

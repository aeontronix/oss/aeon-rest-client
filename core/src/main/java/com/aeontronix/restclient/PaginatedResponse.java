package com.aeontronix.restclient;

import com.aeontronix.restclient.json.JsonConverter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PaginatedResponse<X> implements Iterable<X>, AutoCloseable {
    private final PaginatedIterator iterator = new PaginatedIterator();
    private Iterator<X> pageIterator;
    private final RESTRequest request;
    private final Class<X> objectClass;
    private final RESTResponse response;
    private ResultsPage<X> page;
    private int offset;

    public PaginatedResponse(RESTResponse response, Class<X> objectClass) throws RESTException, ResponseConversionException {
        this.response = response;
        offset = 0;
        request = response.getRequest();
        if (!request.isRepeatable()) {
            throw new IllegalStateException("Pagination can only be used with repeatable requests");
        } else if (!request.isPaginated()) {
            throw new IllegalStateException("Request isn't configured for pagination");
        }
        this.objectClass = objectClass;
        loadPage(response);
    }

    @Override
    public void close() throws IOException {
        response.close();
    }

    public List<X> toList() {
        return StreamSupport.stream(spliterator(), false).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Iterator<X> iterator() {
        return iterator;
    }

    @SuppressWarnings("unchecked")
    private void loadPage(RESTResponse response) throws RESTException, ResponseConversionException {
        final JsonConverter jsonConverter = response.getRequest().getJsonConverter();
        page = jsonConverter.toPage(objectClass, response);
        pageIterator = page.getList().iterator();
    }

    public class PaginatedIterator implements Iterator<X> {
        @Override
        public boolean hasNext() {
            return pageIterator.hasNext();
        }

        @Override
        public X next() {
            try {
                final X value = pageIterator.next();
                if (!pageIterator.hasNext()) {
                    offset += request.getPageSize();
                    request.setPageOffset(offset);
                    loadPage(request.execute());
                }
                return value;
            } catch (RESTException e) {
                throw new PaginationException(e);
            }
        }
    }
}

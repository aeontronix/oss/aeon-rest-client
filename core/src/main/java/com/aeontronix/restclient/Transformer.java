package com.aeontronix.restclient;

public interface Transformer {
    Object transform(Object object);
}

package com.aeontronix.restclient;

import java.net.URI;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class ProxySettings {
    private URI proxyUri;
    private String proxyUsername;
    private String proxyPassword;
    private Set<String> nonProxyHosts;

    public ProxySettings() {
    }

    public ProxySettings(URI proxyUri, String proxyUsername, String proxyPassword, Set<String> nonProxyHosts) {
        this.proxyUri = proxyUri;
        this.proxyUsername = proxyUsername;
        this.proxyPassword = proxyPassword;
        this.nonProxyHosts = nonProxyHosts != null ? nonProxyHosts.stream().map(String::toLowerCase)
                .collect(Collectors.toSet()) : Collections.emptySet();
    }

    public URI getProxyUri() {
        return proxyUri;
    }

    public void setProxyUri(URI proxyUri) {
        this.proxyUri = proxyUri;
    }

    public String getProxyUsername() {
        return proxyUsername;
    }

    public void setProxyUsername(String proxyUsername) {
        this.proxyUsername = proxyUsername;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public Set<String> getNonProxyHosts() {
        return nonProxyHosts;
    }

    public void setNonProxyHosts(Set<String> nonProxyHosts) {
        this.nonProxyHosts = nonProxyHosts;
    }
}

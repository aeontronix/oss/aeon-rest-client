package com.aeontronix.restclient;

import java.net.URI;

public class RESTClientHostBuilder extends RequestParametersBuilder<RESTClientHostBuilder> {
    private final RESTClient restClient;
    private final URI host;

    RESTClientHostBuilder(RESTClient restClient, URI host) {
        this.restClient = restClient;
        this.host = host;
        requestParameters = restClient.getDefaultRequestParameters().clone();
    }

    public RESTClientHost build() {
        return new RESTClientHost(restClient, host, requestParameters);
    }
}

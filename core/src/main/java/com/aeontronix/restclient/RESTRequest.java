package com.aeontronix.restclient;

import com.aeontronix.commons.io.IOUtils;
import com.aeontronix.restclient.errorhandling.RESTAuthenticationException;
import com.aeontronix.restclient.auth.AuthenticationHandler;
import com.aeontronix.restclient.http.HTTPRequest;
import com.aeontronix.restclient.http.HTTPResponse;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

public class RESTRequest extends RequestParameters {
    private static final Logger logger = getLogger(RESTRequest.class);
    private static final long DEBUG_MAX_CONTENT_SIZE = 500000L;
    private final RESTClient restClient;
    private final HTTPRequest request;
    private boolean forceCredentialsRefresh;
    private boolean credentialsRefreshing;
    private boolean credentialsRefreshed;

    public RESTRequest(RESTClient restClient, @NotNull HTTPRequest request, RequestParameters requestParameters) {
        super(requestParameters);
        this.restClient = restClient;
        this.request = request;
    }

    public RESTClient getRestClient() {
        return restClient;
    }

    @NotNull
    public HTTPRequest getRequest() {
        return request;
    }

    public URI getURI() {
        return request.getURI();
    }

    public String getMethod() {
        return request.getMethod();
    }

    public void setURI(URI uri) {
        request.setURI(uri);
    }

    public boolean isBodyAllowed() {
        return request.isBodyAllowed();
    }

    public void setBody(InputStream is) {
        request.setBody(is);
    }

    public void setBody(byte[] data) {
        request.setBody(data);
    }

    public void setHeader(String key, String value) {
        request.setHeader(key, value);
    }

    public boolean isRepeatable() {
        return request.isRepeatable();
    }

    public boolean isForceCredentialsRefresh() {
        return forceCredentialsRefresh;
    }

    public void setForceCredentialsRefresh(boolean forceCredentialsRefresh) {
        this.forceCredentialsRefresh = forceCredentialsRefresh;
    }

    public synchronized void refreshCredentials() throws RESTException {
        if (!credentialsRefreshing) {
            try {
                credentialsRefreshing = true;
                final AuthenticationHandler authenticationHandler = getAuthenticationHandler();
                if (authenticationHandler != null) {
                    try {
                        authenticationHandler.refreshCredential(getRestClient());
                    } catch (IOException e) {
                        throw new RESTException(e);
                    }
                    credentialsRefreshed = true;
                }
            } finally {
                credentialsRefreshing = false;
            }
        }
    }

    public synchronized boolean isCredentialsRefreshable() {
        return !credentialsRefreshed && getAuthenticationHandler() != null &&
                getAuthenticationHandler().isRefreshable() && isRepeatable();
    }

    public synchronized boolean isCredentialsRefreshRequired() {
        final AuthenticationHandler authenticationHandler = getAuthenticationHandler();
        return authenticationHandler != null && authenticationHandler.isRefreshable() && !credentialsRefreshed &&
                (authenticationHandler.isRefreshRequired() || forceCredentialsRefresh);
    }

    public synchronized boolean applyCredentials() throws RESTException {
        boolean refreshed = false;
        final AuthenticationHandler authenticationHandler = getAuthenticationHandler();
        if (authenticationHandler != null) {
            if (isCredentialsRefreshRequired()) {
                refreshCredentials();
                refreshed = true;
            }
            authenticationHandler.applyCredentials(this);
        }
        return refreshed;
    }

    public RESTResponse execute() throws RESTException {
        return getRequestScheduler().executeNow(this, this::doExecute);
    }

    private RESTResponse doExecute() throws RESTException {
        boolean credentialsRefreshed = applyCredentials();
        RESTResponse response = null;
        try {
            RESTRequest restRequest = this;
            if (isPaginated()) {
                restRequest = requireNonNull(getPaginationRequestTransformer(), "Pagination enabled but not pagination transformer set")
                        .transform(this);
            }
            HTTPRequest httpReq = restRequest.getRequest();
            if (logger.isDebugEnabled()) {
                logger.debug(toLogString(httpReq));
            }
            HTTPResponse httpResponse = restClient.getHttpClient().execute(httpReq, this);
            response = new RESTResponse(httpResponse, restRequest);
            if (logger.isDebugEnabled()) {
                logger.debug(response.toLogString());
            }
            final RESTException restException = response.getRequest().getRestClient().getDefaultRequestParameters()
                    .getResponseValidator().validateResponse(response);
            if (restException == null) {
                return response;
            } else if( restException instanceof RESTAuthenticationException) {
                if( ! credentialsRefreshed && isRepeatable() && isCredentialsRefreshable() && ! isForceCredentialsRefresh() ) {
                    logger.debug("Authentication failed, forcing credentials refresh and retrying operation");
                    setForceCredentialsRefresh(true);
                    return doExecute();
                } else {
                    throw restException;
                }
            } else {
                throw restException;
            }
        } catch (IOException e) {
            if( e instanceof RESTException ) {
                throw (RESTException) e;
            } else {
                throw new RESTException(e.getMessage(), e);
            }
        }
    }

    @NotNull
    private static String toLogString(HTTPRequest httpReq) throws IOException {
        StringBuilder reqLog = new StringBuilder("Executing ")
                .append(httpReq.getMethod()).append(" request to ").append(httpReq.getURI())
                .append("\n");
        addHeadersToLog(reqLog, httpReq.getAllHeaders());
        if (httpReq.hasBody()) {
            long size = httpReq.getContentSize();
            if (size < 0 || size > DEBUG_MAX_CONTENT_SIZE) {
                reqLog.append("--- Unable to log payload, size = ");
                reqLog.append(size);
                reqLog.append(" ---\n");
            } else {
                try (InputStream content = httpReq.getContent()) {
                    reqLog.append("---Content---\n");
                    byte[] data = IOUtils.toByteArray(content);
                    if (!httpReq.isRepeatable()) {
                        httpReq.setBody(data);
                    }
                    reqLog.append(new String(data));
                }
            }
        }
        String string = reqLog.toString();
        return string;
    }

    LocalDateTime calcDelay(int retry) {
        final LocalDateTime now = LocalDateTime.now();
        switch (retry) {
            case 0:
                return now.plus(250L, ChronoUnit.MILLIS);
            case 1:
                return now.plus(5, ChronoUnit.SECONDS);
            case 2:
                return now.plus(15, ChronoUnit.SECONDS);
            case 3:
                return now.plus(30, ChronoUnit.SECONDS);
            default:
                return now.plus(1, ChronoUnit.MINUTES);
        }
    }

    static void addHeadersToLog(StringBuilder logs, Map<String, List<String>> headers) {
        logs.append("---Headers---\n");
        if (headers != null) {
            for (Map.Entry<String, List<String>> e : headers.entrySet()) {
                for (String val : e.getValue()) {
                    logs.append(e.getKey()).append(" = ").append(val).append("\n");
                }
            }
        }
    }

    public String getHost() {
        return request.getURI().getHost();
    }
}

package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;

import java.time.LocalDateTime;

public class RESTServerErrorException extends RESTException {
    public RESTServerErrorException(Throwable cause) {
        super(cause);
    }

    public RESTServerErrorException(RESTResponse response) {
        super(response);
    }

    public RESTServerErrorException(RESTResponse response, LocalDateTime retryDelay, boolean globalRetry) {
        super(response, retryDelay, globalRetry);
    }
}

package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;

import java.time.LocalDateTime;

public class RESTBadRequestException extends RESTException {
    public RESTBadRequestException(RESTResponse response) {
        super(response);
    }

    public RESTBadRequestException(RESTResponse response, LocalDateTime retryDelay, boolean globalRetry) {
        super(response, retryDelay, globalRetry);
    }
}

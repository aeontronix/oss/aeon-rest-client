package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTRequest;
import com.aeontronix.restclient.RESTResponse;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class RESTAuthorizationException extends RESTSecurityException {
    public RESTAuthorizationException(Throwable cause) {
        super(cause);
    }

    public RESTAuthorizationException(String message) {
        super(message);
    }

    public RESTAuthorizationException(String message, String reason, Integer statusCode) {
        super(message, reason, statusCode);
    }

    public RESTAuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RESTAuthorizationException(String message, @NotNull RESTResponse response) {
        super(message, response);
    }

    public RESTAuthorizationException(@NotNull RESTResponse response) {
        super(response);
    }

    public RESTAuthorizationException(Integer statusCode, String reason, RESTRequest request, RESTResponse response) {
        super(statusCode, reason, request, response);
    }

    public RESTAuthorizationException(RESTResponse response, LocalDateTime delayRetryUntil, boolean globalRetry) {
        super(response, delayRetryUntil, globalRetry);
    }
}

/*
 * Copyright (c) 2024. Aeontronix Inc
 */

package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTRequest;
import com.aeontronix.restclient.RESTResponse;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class RESTSecurityException extends RESTException {
    public RESTSecurityException(Throwable cause) {
        super(cause);
    }

    public RESTSecurityException(String message) {
        super(message);
    }

    public RESTSecurityException(String message, String reason, Integer statusCode) {
        super(message, reason, statusCode);
    }

    public RESTSecurityException(String message, Throwable cause) {
        super(message, cause);
    }

    public RESTSecurityException(String message, @NotNull RESTResponse response) {
        super(message, response);
    }

    public RESTSecurityException(@NotNull RESTResponse response) {
        super(response);
    }

    public RESTSecurityException(Integer statusCode, String reason, RESTRequest request, RESTResponse response) {
        super(statusCode, reason, request, response);
    }

    public RESTSecurityException(RESTResponse response, LocalDateTime delayRetryUntil, boolean globalRetry) {
        super(response, delayRetryUntil, globalRetry);
    }
}

package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;

public interface RESTResponseValidator {
    RESTException validateResponse(RESTResponse response);
}

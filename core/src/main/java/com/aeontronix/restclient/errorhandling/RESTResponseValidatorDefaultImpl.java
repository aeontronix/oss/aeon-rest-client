package com.aeontronix.restclient.errorhandling;

import com.aeontronix.commons.io.IOUtils;
import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;
import com.aeontronix.restclient.ResponseType;
import com.aeontronix.restclient.json.JsonConverter;
import com.aeontronix.restclient.json.JsonConvertionException;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Map;

public class RESTResponseValidatorDefaultImpl implements RESTResponseValidator {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RESTResponseValidatorDefaultImpl.class);
    private final int unauthenticatedStatusCode;
    private final int unauthorizedStatusCode;
    private final int tooManyCallsStatusCode;

    public RESTResponseValidatorDefaultImpl() {
        this(401, 403, 429);
    }

    public RESTResponseValidatorDefaultImpl(int unauthenticatedStatusCode, int unauthorizedStatusCode, int tooManyCallsStatusCode) {
        this.unauthenticatedStatusCode = unauthenticatedStatusCode;
        this.unauthorizedStatusCode = unauthorizedStatusCode;
        this.tooManyCallsStatusCode = tooManyCallsStatusCode;
    }

    @Override
    public RESTException validateResponse(RESTResponse response) {
        final int statusCode = response.getStatusCode();
        if (statusCode == unauthenticatedStatusCode) {
            return new RESTAuthenticationException(response);
        } else if (statusCode == unauthorizedStatusCode) {
            return new RESTAuthorizationException(response);
        } else if (statusCode == tooManyCallsStatusCode) {
            final LocalDateTime retryAfter = response.getRetryAfter();
            return new RESTTooManyCallsException(response, retryAfter, true);
        } else if (statusCode >= 400 && statusCode < 500) {
            return new RESTBadRequestException(response);
        } else if (statusCode >= 500) {
            final LocalDateTime retryAfter = response.getRetryAfter();
            return new RESTServerErrorException(response, retryAfter, true);
        } else {
            return null;
        }
    }

    public static String findErrorMessage(RESTResponse response) {
        try {
            final InputStream contentStream = response.getContentStream();
            if (response.hasContent() && response.isJson()) {
                JsonConverter jsonConverter = response.getRequest().getJsonConverter();
                if (jsonConverter != null) {
                    final Object obj = jsonConverter.convertFromJson(ResponseType.OBJECT, contentStream, null);
                    if (obj instanceof String) {
                        return (String) obj;
                    } else if (obj instanceof Map) {
                        Map<?, ?> objMap = (Map<?, ?>) obj;
                        Object mapMsg = objMap.get("message");
                        if (mapMsg instanceof String) {
                            String message = (String) mapMsg;
                            Object details = objMap.get("details");
                            if (details != null) {
                                try {
                                    message = message + " : " + jsonConverter.convertToJsonString(details);
                                } catch (JsonConvertionException e) {
                                    logger.debug(e.getMessage(), e);
                                }
                            }
                            return message;
                        }
                    }
                } else {
                    return IOUtils.toString(contentStream);
                }
            }
        } catch (IOException e) {
            logger.debug("Unable to convert error response payload", e);
        }
        return response.getStatusReasons();
    }
}

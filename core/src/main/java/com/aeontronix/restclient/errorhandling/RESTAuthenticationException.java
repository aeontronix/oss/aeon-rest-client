/*
 * Copyright (c) 2024. Aeontronix Inc
 */

package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTRequest;
import com.aeontronix.restclient.RESTResponse;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class RESTAuthenticationException extends RESTSecurityException {
    public RESTAuthenticationException(Throwable cause) {
        super(cause);
    }

    public RESTAuthenticationException(String message) {
        super(message);
    }

    public RESTAuthenticationException(String message, String reason, Integer statusCode) {
        super(message, reason, statusCode);
    }

    public RESTAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RESTAuthenticationException(String message, @NotNull RESTResponse response) {
        super(message, response);
    }

    public RESTAuthenticationException(@NotNull RESTResponse response) {
        super(response);
    }

    public RESTAuthenticationException(Integer statusCode, String reason, RESTRequest request, RESTResponse response) {
        super(statusCode, reason, request, response);
    }

    public RESTAuthenticationException(RESTResponse response, LocalDateTime delayRetryUntil, boolean globalRetry) {
        super(response, delayRetryUntil, globalRetry);
    }
}

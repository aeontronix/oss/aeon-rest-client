package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class RESTTooManyCallsException extends RESTException {
    public RESTTooManyCallsException(Throwable cause) {
        super(cause);
    }

    public RESTTooManyCallsException(@NotNull RESTResponse response) {
        super(response);
    }

    public RESTTooManyCallsException(RESTResponse response, LocalDateTime retryDelay, boolean globalRetry) {
        super(response, retryDelay, globalRetry);
    }
}

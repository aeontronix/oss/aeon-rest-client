package com.aeontronix.restclient.errorhandling;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.RESTResponse;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class RESTServiceUnavailableException extends RESTException {
    public RESTServiceUnavailableException(Throwable cause) {
        super(cause);
    }

    public RESTServiceUnavailableException(@NotNull RESTResponse response) {
        super(response);
    }

    public RESTServiceUnavailableException(RESTResponse response, LocalDateTime retryDelay, boolean globalRetry) {
        super(response, retryDelay, globalRetry);
    }
}

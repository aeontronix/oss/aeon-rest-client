package com.aeontronix.restclient;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class ResponseConversionException extends RESTException {
    public ResponseConversionException(Throwable cause) {
        super(cause);
    }

    public ResponseConversionException(String message) {
        super(message);
    }

    public ResponseConversionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResponseConversionException(@NotNull RESTResponse response) {
        super(response);
    }

    public ResponseConversionException(RESTResponse response, LocalDateTime retryDelay, boolean globalRetry) {
        super(response, retryDelay, globalRetry);
    }
}

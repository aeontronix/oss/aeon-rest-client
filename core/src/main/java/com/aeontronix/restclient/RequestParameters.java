package com.aeontronix.restclient;

import com.aeontronix.commons.exception.UnexpectedException;
import com.aeontronix.restclient.auth.AuthenticationHandler;
import com.aeontronix.restclient.errorhandling.RESTResponseValidator;
import com.aeontronix.restclient.errorhandling.RESTResponseValidatorDefaultImpl;
import com.aeontronix.restclient.json.JsonConverter;

import java.time.Duration;
import java.util.Map;

public class RequestParameters implements Cloneable {
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    private AuthenticationHandler authenticationHandler;
    private RESTRequestScheduler requestScheduler = new RESTRequestSchedulerDefaultImpl();
    private JsonConverter jsonConverter;
    private boolean laxJsonConverter;
    private Integer maxRetries;
    private Duration retryDelay;
    private Transformer responseTransformer;
    private RESTResponseValidator responseValidator = new RESTResponseValidatorDefaultImpl();
    private int pageOffset;
    private int pageSize = 50;
    private String pageOffsetQueryParameterName = OFFSET;
    private String pageSizeQueryParameterName = LIMIT;
    private PaginationRequestTransformer paginationRequestTransformer;
    private boolean paginated;
    private String pageValuesPath = "/values";
    private Integer connectionTimeout;
    private Integer socketTimeout;
    private Integer connectionPoolTimeout;
    protected Map<Object, Object> objectMapperInjections;

    public RequestParameters() {
    }

    public RequestParameters(RequestParameters requestParameters) {
        this(requestParameters.authenticationHandler, requestParameters.requestScheduler,
                requestParameters.jsonConverter, requestParameters.laxJsonConverter,
                requestParameters.maxRetries, requestParameters.retryDelay,
                requestParameters.responseValidator, requestParameters.pageOffset, requestParameters.pageSize,
                requestParameters.pageOffsetQueryParameterName, requestParameters.pageSizeQueryParameterName,
                requestParameters.paginationRequestTransformer, requestParameters.paginated,
                requestParameters.pageValuesPath, requestParameters.connectionTimeout, requestParameters.socketTimeout,
                requestParameters.connectionPoolTimeout, requestParameters.objectMapperInjections);
    }

    public RequestParameters(AuthenticationHandler authenticationHandler,
                             RESTRequestScheduler requestScheduler, JsonConverter jsonConverter, boolean laxJsonConverter,
                             Integer maxRetries, Duration retryDelay,
                             RESTResponseValidator responseValidator, int pageOffset, int pageSize,
                             String pageOffsetQueryParameterName, String pageSizeQueryParameterName,
                             PaginationRequestTransformer paginationRequestTransformer, boolean paginated,
                             String pageValuesPath, Integer connectionTimeout, Integer socketTimeout,
                             Integer connectionPoolTimeout, Map<Object, Object> objectMapperInjections) {
        this.authenticationHandler = authenticationHandler;
        this.requestScheduler = requestScheduler;
        this.jsonConverter = jsonConverter;
        this.laxJsonConverter = laxJsonConverter;
        this.maxRetries = maxRetries;
        this.retryDelay = retryDelay;
        this.responseValidator = responseValidator;
        this.pageOffset = pageOffset;
        this.pageSize = pageSize;
        this.pageOffsetQueryParameterName = pageOffsetQueryParameterName;
        this.pageSizeQueryParameterName = pageSizeQueryParameterName;
        this.paginationRequestTransformer = paginationRequestTransformer;
        this.paginated = paginated;
        this.pageValuesPath = pageValuesPath;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
        this.connectionPoolTimeout = connectionPoolTimeout;
        this.objectMapperInjections = objectMapperInjections;
    }

    @Override
    public RequestParameters clone() {
        try {
            return (RequestParameters) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new UnexpectedException(e);
        }
    }

    public AuthenticationHandler getAuthenticationHandler() {
        return authenticationHandler;
    }

    public void setAuthenticationHandler(AuthenticationHandler authenticationHandler) {
        this.authenticationHandler = authenticationHandler;
    }

    public JsonConverter getJsonConverter() {
        return jsonConverter;
    }

    public void setJsonConverter(JsonConverter jsonConverter) {
        this.jsonConverter = jsonConverter;
    }

    public boolean isLaxJsonConverter() {
        return laxJsonConverter;
    }

    public void setLaxJsonConverter(boolean laxJsonConverter) {
        this.laxJsonConverter = laxJsonConverter;
    }

    public Map<Object, Object> getObjectMapperInjections() {
        return objectMapperInjections;
    }

    public void setObjectMapperInjections(Map<Object, Object> objectMapperInjections) {
        this.objectMapperInjections = objectMapperInjections;
    }

    public Integer getMaxRetries() {
        return maxRetries;
    }

    public Duration getRetryDelay() {
        return retryDelay;
    }

    public void setRetryDelay(Duration retryDelay) {
        this.retryDelay = retryDelay;
    }

    public void setMaxRetries(Integer maxRetries) {
        this.maxRetries = maxRetries;
    }

    public Transformer getResponseTransformer() {
        return responseTransformer;
    }

    public void setResponseTransformer(Transformer responseTransformer) {
        this.responseTransformer = responseTransformer;
    }

    public RESTResponseValidator getResponseValidator() {
        return responseValidator;
    }

    public void setResponseValidator(RESTResponseValidator responseValidator) {
        this.responseValidator = responseValidator;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageOffsetQueryParameterName() {
        return pageOffsetQueryParameterName;
    }

    public void setPageOffsetQueryParameterName(String pageOffsetQueryParameterName) {
        this.pageOffsetQueryParameterName = pageOffsetQueryParameterName;
    }

    public String getPageSizeQueryParameterName() {
        return pageSizeQueryParameterName;
    }

    public void setPageSizeQueryParameterName(String pageSizeQueryParameterName) {
        this.pageSizeQueryParameterName = pageSizeQueryParameterName;
    }

    public PaginationRequestTransformer getPaginationRequestTransformer() {
        return paginationRequestTransformer;
    }

    public void setPaginationRequestTransformer(PaginationRequestTransformer paginationRequestTransformer) {
        this.paginationRequestTransformer = paginationRequestTransformer;
    }

    public boolean isPaginated() {
        return paginated;
    }

    public void setPaginated(boolean paginated) {
        this.paginated = paginated;
        if (paginated && paginationRequestTransformer == null) {
            paginationRequestTransformer = new PaginationRequestTransformerDefaultImpl();
        }
    }

    public String getPageValuesPath() {
        return pageValuesPath;
    }

    public void setPageValuesPath(String pageValuesPath) {
        this.pageValuesPath = pageValuesPath;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    public RESTRequestScheduler getRequestScheduler() {
        return requestScheduler;
    }

    public void setRequestScheduler(RESTRequestScheduler requestScheduler) {
        this.requestScheduler = requestScheduler;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public Integer getConnectionPoolTimeout() {
        return connectionPoolTimeout;
    }

    public void setConnectionPoolTimeout(Integer connectionPoolTimeout) {
        this.connectionPoolTimeout = connectionPoolTimeout;
    }
}

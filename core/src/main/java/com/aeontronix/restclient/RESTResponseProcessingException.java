/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient;

public class RESTResponseProcessingException extends Exception {
    public RESTResponseProcessingException() {
    }

    public RESTResponseProcessingException(String message) {
        super(message);
    }

    public RESTResponseProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public RESTResponseProcessingException(Throwable cause) {
        super(cause);
    }

    public RESTResponseProcessingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

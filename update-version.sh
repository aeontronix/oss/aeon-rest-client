#!/bin/bash

echo "Latest tag: $(git describe --abbrev=0 --tags)"
echo "Current version: $( xmlstarlet sel -N 'p=http://maven.apache.org/POM/4.0.0' -t -v '/p:project/p:version/text()' pom.xml | sed 's/-SNAPSHOT$//' )"
echo "New version ?"
read VERSION
./mvnw versions:set -DprocessAllModules=true -DgroupId='*' -DartifactId='*' -DoldVersion='*' -DnewVersion=${VERSION}-SNAPSHOT -DgenerateBackupPoms=false

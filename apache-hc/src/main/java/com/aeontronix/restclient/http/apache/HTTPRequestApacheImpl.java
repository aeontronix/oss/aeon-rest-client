package com.aeontronix.restclient.http.apache;

import com.aeontronix.restclient.http.HTTPRequest;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HTTPRequestApacheImpl implements HTTPRequest {
    HttpRequestBase request;

    public HTTPRequestApacheImpl(HttpRequestBase request) {
        this.request = request;
    }

    @Override
    public URI getURI() {
        return request.getURI();
    }

    @Override
    public void setURI(URI uri) {
        request.setURI(uri);
    }

    @Override
    public boolean isBodyAllowed() {
        return request instanceof HttpEntityEnclosingRequestBase;
    }

    @Override
    public void setBody(InputStream is) {
        ((HttpEntityEnclosingRequestBase) request).setEntity(new InputStreamEntity(is));
    }

    @Override
    public void setBody(byte[] data) {
        ((HttpEntityEnclosingRequestBase) request).setEntity(new ByteArrayEntity(data));
    }

    @Override
    public boolean hasBody() {
        if(request instanceof HttpEntityEnclosingRequestBase) {
            return ((HttpEntityEnclosingRequestBase) request).getEntity() != null;
        }
        return false;
    }

    @Nullable
    @Override
    public InputStream getContent() throws IOException {
        HttpEntity entity = getEntity();
        if( entity != null ) {
            return entity.getContent();
        }
        return null;
    }

    @Override
    public long getContentSize() {
        HttpEntity entity = getEntity();
        if( entity != null ) {
            return entity.getContentLength();
        }
        return -1;
    }

    private HttpEntity getEntity() {
        if( request instanceof HttpEntityEnclosingRequestBase ) {
            HttpEntity entity = ((HttpEntityEnclosingRequestBase) request).getEntity();
            if (entity != null) {
                return entity;
            }
        }
        return null;
    }

    @Override
    public void setHeader(String key, String value) {
        request.setHeader(key, value);
    }

    @Override
    @NotNull
    public Map<String, List<String>> getAllHeaders() {
        HashMap<String, List<String>> headers = new HashMap<>();
        Header[] allHeaders = request.getAllHeaders();
        if( allHeaders != null ) {
            for (Header header : allHeaders) {
                headers.computeIfAbsent(header.getName(), s -> new ArrayList<>()).add(header.getValue());
            }
        }
        return headers;
    }

    @Override
    public boolean isRepeatable() {
        if (request instanceof HttpEntityEnclosingRequestBase) {
            final HttpEntity entity = ((HttpEntityEnclosingRequestBase) request).getEntity();
            if (entity != null) {
                return entity.isRepeatable();
            }
        }
        return true;
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }
}

package com.aeontronix.restclient.http.apache;

import com.aeontronix.commons.exception.UnexpectedException;
import com.aeontronix.restclient.ProxySettings;
import com.aeontronix.restclient.RESTClient;
import com.aeontronix.restclient.RequestParameters;
import com.aeontronix.restclient.http.HTTPClient;
import com.aeontronix.restclient.http.HTTPRequest;
import com.aeontronix.restclient.http.HTTPResponse;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.config.Registry;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class HTTPClientApacheImpl implements HTTPClient {
    private final CloseableHttpClient httpClient;

    public HTTPClientApacheImpl(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public HTTPClientApacheImpl(RESTClient.Builder rb, boolean insecure, Integer connectionTimeout) {
        try {
            RequestConfig.Builder customCfg = RequestConfig.custom();
            final RequestParameters rp = rb.getRequestParameters();
            if (connectionTimeout != null) {
                customCfg = customCfg.setConnectionRequestTimeout(connectionTimeout);
            }
            if (rp.getSocketTimeout() != null) {
                customCfg = customCfg.setSocketTimeout(rp.getSocketTimeout());
            }
            if (rp.getConnectionPoolTimeout() != null) {
                customCfg = customCfg.setConnectionRequestTimeout(rp.getConnectionPoolTimeout());
            }
            HttpClientBuilder builder = HttpClientBuilder.create()
                    .useSystemProperties()
                    .setDefaultRequestConfig(customCfg.build())
                    .setMaxConnTotal(rb.getMaxConnTotal())
                    .setMaxConnPerRoute(rb.getMaxConnPerRoute());
            if (insecure) {
                SSLContextBuilder sslContextBuilder = new SSLContextBuilder();
                sslContextBuilder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContextBuilder.build(),new NoopHostnameVerifier());
//                Registry<ConnectionSocketFactory> registry = RegistryBuilder.
//                        <ConnectionSocketFactory> create()
//                        .register("http", new PlainConnectionSocketFactory())
//                        .register("https", sslsf)
//                        .build();
                builder.setSSLSocketFactory(sslsf);
            }
            if (!rb.isCookies()) {
                builder.disableCookieManagement();
            }
            final ProxySettings proxySettings = rb.getProxySettings();
            if (proxySettings != null) {
                HttpHost proxyHost = new HttpHost(proxySettings.getProxyUri().toString());
                DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxyHost) {
                    @Override
                    public HttpRoute determineRoute(HttpHost host, HttpRequest request, HttpContext context) throws HttpException {
                        String hostname = host.getHostName();
                        if (proxySettings.getNonProxyHosts().contains(hostname.toLowerCase())) {
                            return new HttpRoute(host);
                        }
                        return super.determineRoute(host, request, context);
                    }
                };
                if (proxySettings.getProxyUsername() != null || proxySettings.getProxyPassword() != null) {
                    CredentialsProvider credsProvider = new BasicCredentialsProvider();
                    credsProvider.setCredentials(new AuthScope(proxyHost), new UsernamePasswordCredentials(proxySettings.getProxyUsername(), proxySettings.getProxyPassword()));
                    builder.setDefaultCredentialsProvider(credsProvider);
                }
                builder.setRoutePlanner(routePlanner);
            }
            httpClient = builder.build();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            throw new UnexpectedException(e);
        }
    }

    @Override
    @NotNull
    public HTTPResponse execute(HTTPRequest request, RequestParameters requestParameters) throws IOException {
        final HttpRequestBase httpClientRequest = ((HTTPRequestApacheImpl) request).request;
        final RequestConfig.Builder config = RequestConfig.custom();
        if (requestParameters.getConnectionTimeout() != null) {
            config.setConnectTimeout(requestParameters.getConnectionTimeout());
        }
        if (requestParameters.getSocketTimeout() != null) {
            config.setSocketTimeout(requestParameters.getSocketTimeout());
        }
        if (requestParameters.getConnectionPoolTimeout() != null) {
            config.setConnectionRequestTimeout(requestParameters.getConnectionPoolTimeout());
        }
        httpClientRequest.setConfig(config.build());
        final CloseableHttpResponse response = httpClient.execute(httpClientRequest);
        return new HTTPResponseApacheImpl(response);
    }

    @Override
    @NotNull
    public HTTPRequest createGetRequest() {
        return new HTTPRequestApacheImpl(new HttpGet());
    }

    @Override
    @NotNull
    public HTTPRequest createPostRequest() {
        return new HTTPRequestApacheImpl(new HttpPost());
    }

    @Override
    @NotNull
    public HTTPRequest createPutRequest() {
        return new HTTPRequestApacheImpl(new HttpPut());
    }

    @Override
    @NotNull
    public HTTPRequest createHeadRequest() {
        return new HTTPRequestApacheImpl(new HttpHead());
    }

    @Override
    @NotNull
    public HTTPRequest createDeleteRequest() {
        return new HTTPRequestApacheImpl(new HttpDelete());
    }

    @Override
    @NotNull
    public HTTPRequest createPatchRequest() {
        return new HTTPRequestApacheImpl(new HttpPatch());
    }

    @Override
    @NotNull
    public HTTPRequest createOptionsRequest() {
        return new HTTPRequestApacheImpl(new HttpOptions());
    }

    @Override
    @NotNull
    public HTTPRequest createTraceRequest() {
        return new HTTPRequestApacheImpl(new HttpTrace());
    }

    @Override
    @NotNull
    public HTTPRequest createRequest(@NotNull String method) {
        return new HTTPRequestApacheImpl(new HttpEntityEnclosingRequestBase() {
            @Override
            public String getMethod() {
                return method;
            }
        });
    }
}

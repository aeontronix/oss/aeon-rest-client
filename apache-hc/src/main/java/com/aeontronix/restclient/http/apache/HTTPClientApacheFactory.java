package com.aeontronix.restclient.http.apache;

import com.aeontronix.restclient.RESTClient;
import com.aeontronix.restclient.http.HTTPClient;
import com.aeontronix.restclient.http.HTTPClientFactory;

public class HTTPClientApacheFactory implements HTTPClientFactory {
    @Override
    public HTTPClient create(RESTClient.Builder builder, boolean insecure) {
        return new HTTPClientApacheImpl(builder, insecure,
                builder.getRequestParameters().getConnectionTimeout());
    }
}

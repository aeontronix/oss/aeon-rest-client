package com.aeontronix.restclient.http.apache;

import com.aeontronix.restclient.RESTException;
import com.aeontronix.restclient.http.HTTPResponse;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class HTTPResponseApacheImpl implements HTTPResponse {
    private final CloseableHttpResponse response;

    public HTTPResponseApacheImpl(CloseableHttpResponse response) {
        this.response = response;
    }

    @Override
    public int getStatusCode() {
        return response.getStatusLine().getStatusCode();
    }

    @Override
    public String getStatusReasons() {
        return response.getStatusLine().getReasonPhrase();
    }

    @Override
    public String getStatusProtocolVersion() {
        return response.getStatusLine().getProtocolVersion().toString();
    }

    @Override
    public boolean hasContent() {
        return response.getEntity() != null;
    }

    @Override
    public InputStream getContentStream() throws RESTException {
        try {
            return response.getEntity().getContent();
        } catch (IOException e) {
            throw new RESTException(e.getMessage(),e);
        }
    }

    @Override
    public long getContentLength() {
        final HttpEntity entity = response.getEntity();
        return entity != null ? entity.getContentLength() : -1;
    }

    @Override
    public String getContentEncoding() {
        final HttpEntity entity = response.getEntity();
        if (entity != null) {
            final Header contentEncoding = entity.getContentEncoding();
            if (contentEncoding != null) {
                return contentEncoding.getValue();
            }
        }
        return null;
    }

    @Override
    public String getHeader(String name) {
        final Header header = response.getFirstHeader(name);
        return header != null ? header.getValue() : null;
    }

    @Override
    public List<String> getHeaders(String name) {
        final Header[] headers = response.getHeaders(name);
        if (headers != null) {
            return Arrays.stream(headers).map(NameValuePair::getValue).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Map<String, List<String>> getAllHeaders() {
        Map<String, List<String>> results = new HashMap<>();
        for (Header header : response.getAllHeaders()) {
            results.computeIfAbsent(header.getName(), s -> new ArrayList<>()).add(header.getValue());
        }
        return results;
    }

    @Override
    public boolean isJson() {
        return "application/json".equalsIgnoreCase(getContentMimeType());
    }

    @Override
    public String getContentMimeType() {
        final HttpEntity entity = response.getEntity();
        if (entity != null) {
            final ContentType contentType = ContentType.get(entity);
            if( contentType != null ) {
                return contentType.getMimeType();
            }
        }
        return null;
    }

    @Override
    public void close() throws IOException {
        response.close();
    }
}

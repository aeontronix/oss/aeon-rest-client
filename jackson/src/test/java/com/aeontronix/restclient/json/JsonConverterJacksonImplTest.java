package com.aeontronix.restclient.json;

import com.aeontronix.restclient.*;
import com.aeontronix.restclient.http.HTTPClient;
import com.aeontronix.restclient.http.HTTPRequest;
import com.aeontronix.restclient.http.HTTPResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class JsonConverterJacksonImplTest {
    public static final String API_URL = "http://myapi";
    private HTTPClient httpClient;
    private RESTClient restClient;
    private HTTPRequest request;
    private HTTPResponse response;
    private JsonConverter jsonConverter;

    @BeforeEach
    public void init() {
        httpClient = Mockito.mock(HTTPClient.class);
        response = Mockito.mock(HTTPResponse.class);
        request = Mockito.mock(HTTPRequest.class);
        when(request.getURI()).thenReturn(URI.create("https://myapi"));
        final RequestParameters defaultRequestParameters = new RequestParameters();
        jsonConverter = new JsonConverterJacksonFactory().build(false, null);
        defaultRequestParameters.setJsonConverter(jsonConverter);
        restClient = new RESTClient(httpClient, null, defaultRequestParameters);
        when(httpClient.createGetRequest()).thenReturn(request);
    }

    public void mockResponse(int statusCode, String content, boolean json) throws Exception {
        when(response.getStatusCode()).thenReturn(statusCode);
        mockResponse(content, json);
    }

    public void mockResponse(String content, boolean json) throws Exception {
        when(response.hasContent()).thenReturn(content != null);
        when(response.isJson()).thenReturn(content != null && json);
        if (content != null) {
            when(response.getContentStream()).thenReturn(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
            if (json) {
                when(response.getHeader("Content-Type")).thenReturn("application/json");
            }
        } else {
            when(response.getContentStream()).thenReturn(new ByteArrayInputStream(new byte[0]));
        }
        when(httpClient.execute(any(HTTPRequest.class), any(RequestParameters.class))).thenReturn(response);
    }

    private void verifyResponseClose(int times) throws IOException {
        Mockito.verify(this.response, times(times)).close();
    }

    @Test
    public void testJsonErrorResponse() throws Exception {
        mockResponse(400, "{ \"message\":\"is broken\"}", true);
        try {
            try (RESTResponse response = restClient.get("https://myapi").execute()) {
                Assertions.fail("Request didn't fail as expected");
            }
        } catch (RESTException e) {
            assertEquals("Server returned status code 400 : is broken", e.getMessage());
        }
        verifyResponseClose(1);
    }


    @Test
    public void testJsonPagination() throws Exception {
        mockResponse(200, "{ \"xval\": [\"foo\",\"bar\"] }", true);
        when(response.getContentStream()).thenReturn(
                new ByteArrayInputStream("{ \"xval\": [\"foo\",\"bar\"] }".getBytes(StandardCharsets.UTF_8)),
                new ByteArrayInputStream("{ \"xval\": [\"baz\"] }".getBytes(StandardCharsets.UTF_8)),
                new ByteArrayInputStream("{ \"xval\": [] }".getBytes(StandardCharsets.UTF_8))
        );
        when(request.getURI()).thenReturn(URI.create(API_URL));
        when(request.isRepeatable()).thenReturn(true);
        try (final PaginatedResponse<String> r = restClient.get(API_URL).executePaginated(String.class, "offset", "limit", "/xval")) {
            final List<String> results = r.toList();
            assertEquals(Arrays.asList("foo", "bar", "baz"), results);
        }
    }

    @Test
    public void testGetJsonString() throws Exception {
        mockResponse(200, "\"foo\"", true);
        final RESTResponse response = restClient.get("https://myapi").execute();
        assertEquals("foo", response.toObject(Object.class));
        verifyResponseClose(1);
    }

    @Test
    public void testInjectionByClass() throws RESTException {
        HashMap<Object, Object> injectables = new HashMap<>();
        Injectable injectable = new Injectable();
        injectables.put(Injectable.class, injectable);
        JsonConverter c = new JsonConverterJacksonFactory().build(false, injectables);
        JsonObjectWithInjectionByClass obj = c.convertFromJson(ResponseType.object(JsonObjectWithInjectionByClass.class), new ByteArrayInputStream("{}".getBytes()), null);
        assertSame(injectable,obj.getInjectable());
    }

    @Test
    public void testInjectionByString() throws RESTException {
        HashMap<Object, Object> injectables = new HashMap<>();
        Injectable injectable = new Injectable();
        injectables.put("doinject", injectable);
        JsonConverter c = new JsonConverterJacksonFactory().build(false, injectables);
        JsonObjectWithInjectionById obj = c.convertFromJson(ResponseType.object(JsonObjectWithInjectionById.class), new ByteArrayInputStream("{}".getBytes()), null);
        assertSame(injectable,obj.getInjectable());
    }
}

/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.restclient.json;

import com.fasterxml.jackson.annotation.JacksonInject;

public class JsonObjectWithInjectionByClass {
    @JacksonInject()
    private Injectable injectable;

    public Injectable getInjectable() {
        return injectable;
    }

    public void setInjectable(Injectable injectable) {
        this.injectable = injectable;
    }
}

/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient.json;

import com.aeontronix.restclient.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class JsonConverterJacksonImpl implements JsonConverter {
    public ObjectMapper objectMapper;

    public JsonConverterJacksonImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public byte[] convertToJson(Object object) throws JsonConvertionException {
        try {
            return objectMapper.writeValueAsBytes(object);
        } catch (JsonProcessingException e) {
            throw new JsonConvertionException(e);
        }
    }

    @Override
    public String convertToJsonString(Object object) throws JsonConvertionException {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new JsonConvertionException(e);
        }
    }

    @Override
    public <X> X convertFromJson(ResponseType<X> responseType, InputStream is, String jsonPointerPath) throws RESTException, ResponseConversionException {
        try {
            if( jsonPointerPath != null ) {
                JsonNode rootNode = objectMapper.readTree(is);
                JsonNode dataPath = rootNode.at(jsonPointerPath);
                switch (responseType.getType()) {
                    case OBJECT:
                        return objectMapper.readerFor(responseType.getClassType()).readValue(dataPath);
                    case LIST:
                        return objectMapper.readerForListOf(responseType.getClassType()).readValue(dataPath);
                    default:
                        throw new IllegalArgumentException("Invalid ResponseType type: "+responseType.getType());
                }
            } else {
                switch (responseType.getType()) {
                    case OBJECT:
                        return objectMapper.readValue(is, responseType.getClassType());
                    case LIST:
                        return objectMapper.readerForListOf(responseType.getClassType()).readValue(is);
                    default:
                        throw new IllegalArgumentException("Invalid ResponseType type: "+responseType.getType());
                }
            }
        } catch (DatabindException e) {
            throw new ResponseConversionException(e);
        } catch (IOException e) {
            throw new RESTException(e.getMessage(),e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <X> ResultsPage toPage(Class<X> classType, RESTResponse response) throws RESTException, ResponseConversionException {
        try {
            final JsonNode root = objectMapper.readTree(response.getContentStream());
            final JsonNode values = root.at(response.getRequest().getPageValuesPath());
            return new ResultsPage<X>(objectMapper.readerForListOf(classType).treeToValue(values, objectMapper.getTypeFactory().constructCollectionType(List.class,classType)));
        } catch (DatabindException e) {
            throw new ResponseConversionException(e);
        } catch (IOException e) {
            throw new RESTException(e.getMessage(),e);
        }
    }
}

/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.restclient.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import java.util.Map;

public class JsonConverterJacksonFactory implements JsonConverterFactory {
    @Override
    public JsonConverter build(boolean lax, Map<Object, Object> objectMapperInjections) {
        JsonFactory jsonFactory = JsonFactory.builder()
                .build();
        JsonMapper.Builder jmBuilder = JsonMapper.builder(jsonFactory);
        if (lax) {
            jmBuilder = jmBuilder.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            jmBuilder.serializationInclusion(JsonInclude.Include.NON_NULL);
        }
        if( objectMapperInjections != null && !objectMapperInjections.isEmpty() ) {
            InjectableValues.Std std = new InjectableValues.Std();
            for (Map.Entry<Object, Object> e : objectMapperInjections.entrySet()) {
                Object key = e.getKey();
                if( key instanceof String) {
                    std.addValue((String) key,e.getValue());
                } else if( key instanceof Class<?> ) {
                    std.addValue((Class<?>) key,e.getValue());
                } else {
                    throw new IllegalArgumentException("Injection keys can only be String or Class");
                }
            }
            jmBuilder.injectableValues(std);
        }
        return new JsonConverterJacksonImpl(jmBuilder
                .addModules(new JavaTimeModule())
                .addModules(new Jdk8Module())
                .addModules(new ParameterNamesModule())
                .build());
    }
}
